import { REACT_APP_MAPS_API_KEY } from 'react-native-dotenv';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';

export const getCurrentLocationApi = () => { 
    const options =  {  enableHighAccuracy: true, 
                        timeout: 10000, 
                        maximumAge: 10000 
                     } 

   return new Promise((resolve, reject) => {
        Geolocation.getCurrentPosition(resolve, reject, options)
    });
};

export const getAddressFromCoordinatesApi = (latitude, longitude) => {
    Geocoder.init(`${REACT_APP_MAPS_API_KEY}`);
    const parsedAdress = Geocoder.from(latitude, longitude)
                .then(json => {
                    var address = json.results[0].formatted_address;
                    return address
                })
                .catch(error => reject(error));
    return parsedAdress;
};