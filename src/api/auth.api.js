import axios from 'axios';
import { REACT_APP_OKILI_BACK } from 'react-native-dotenv';

export const signupApi = (user) => {
    const url = `${REACT_APP_OKILI_BACK}/auth/signup`;
    var formData = new FormData();
    
    if (user.picture_user !== undefined){
            const image = {
                type : "image/jpg",
                uri : user.picture_user.uri, 
                name :user.picture_user.fileName
            }
        formData.append( "picture_user", image);
    }
    formData.append( "firstname",    user.firstname);
    formData.append( "password",     user.password);
    formData.append( "lastname",     user.lastname);
    formData.append( "email",        user.email);
    formData.append( "phone_number", user.phone_number);
    formData.append( "address",      user.address);

    const request = new Promise((resolve, reject) => {
        axios.post(url, formData)
              .then(({data}) => {
                resolve(data)
              })
              .catch(error => {
                if (error.response) {
                    // Request made and server responded
                    console.log('data', error.response.data);
                    console.log('status', error.response.status);
                    console.log('headers', error.response.headers);
                    return error.response
                    } else if (error.request) {
                        // The request was made but no response was received
                        console.log('request', error.request);
                        return error.request
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        console.log('Error', reject(error.message));
                    };
                });
    })
    return request;
}

export const loginApi = user => {
    const url = `${REACT_APP_OKILI_BACK}/auth/login`;
    const data = user;
    const options = {
        headers : { 
            'Accept' : 'application/json',
            'Content-Type': 'application/json;charset=UTF-8',
        }
    };

    const request = new Promise((resolve, reject) => {
        axios.post(url, data, options)
            .then(({data}) => {
                resolve(data);
            })
            .catch(err => { 
                reject(err.response.data.message);
            });
    });
    return request;
};