import axios from 'axios';
import { REACT_APP_OKILI_BACK } from 'react-native-dotenv';

export const getUserApi = (id) => {
    const url = `${REACT_APP_OKILI_BACK}/user/${id}`;
    const options = {
        headers : {
        'Accept' : 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
        }
    };
    return axios.get(url, options)
                .then(({data}) => {
                    if (data) return data
                    else return err
                })
};