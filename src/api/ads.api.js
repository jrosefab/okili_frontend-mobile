import axios from 'axios';
import { REACT_APP_OKILI_BACK } from 'react-native-dotenv';

axios.defaults.withCredentials = true;

export const getRecentAdsApi = () => {
    const url = `${REACT_APP_OKILI_BACK}/ads`;
    const options = { 
        headers : {'Accept' : 'application/json'}
    };
    return axios.get(url, options)
                .then(({data}, err) => {
                if (data) return data
                else {
                    console.log(err)
                    return err
                }
            })
};

export const createAdApi = newAd => {
    const url = `${REACT_APP_OKILI_BACK}/ad/create`;
    const image = { 
        type : "image/jpg",
         uri : newAd.picture_publication.uri, 
         name : newAd.picture_publication.fileName
    };

    var formData = new FormData();
    formData.append( "picture_publication",     image);
    formData.append( "kind",                    newAd.kind);
    formData.append( "category",                newAd.category);
    formData.append( "title",                   newAd.title);
    formData.append( "home_delivery",           newAd.home_delivery);
    formData.append( "home_pickup",             newAd.home_pickup);
    formData.append( "home_pickup_details",     newAd.home_pickup_details);
    formData.append( "delivery_km_acceptation", newAd.delivery_km_acceptation);
    formData.append( "tags",                    newAd.tags);
    formData.append( "details_content",         newAd.details_content);
    formData.append( "quantity_left",           newAd.quantity_left);
    formData.append( "condition",               newAd.condition);
    formData.append( "urgency",                 newAd.urgency);
    formData.append( "address",                 newAd.geolocation.address);
    formData.append( "longitude",               newAd.geolocation.longitude);
    formData.append( "latitude",                newAd.geolocation.latitude);
    formData.append( "price",                   newAd.price);
    
    return axios.post(url, formData)
                .then(({data}) => {
                    return data
                })
                .catch(error => {
                    if (error.response) {
                        // Request made and server responded
                        console.log('data', error.response.data);
                        console.log('status', error.response.status);
                        console.log('headers', error.response.headers);
                        return error.response
                        } else if (error.request) {
                            // The request was made but no response was received
                            console.log('request', error.request);
                            return error.request
                        } else {
                            // Something happened in setting up the request that triggered an Error
                            console.log('Error', error.message);
                        };
                });
};

export const getMyAdListApi = (user) => {
    const url = `${REACT_APP_OKILI_BACK}/ads/${user}`;
    const options = { 
        headers : {'Accept' : 'application/json'}
    };
    return axios.get(url, options)
                .then(({data}, err) => {
                if (data) return data
                else {
                    console.log(err)
                    return err
                }
            })
};

export const getSingleAdApi = (adId) => {
    const url = `${REACT_APP_OKILI_BACK}/ad/${adId}`;
    const options = { 
        headers : {'Accept' : 'application/json'}
    };
    return axios.get(url, options)
                .then(({data}, err) => {
                if (data) return data
                else {
                    console.log(err)
                    return err
                }
            })
};