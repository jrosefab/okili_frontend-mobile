import { AUTH_CONSTANTS } from '../constants/auth.constants';

const initialState = {};

export default function authReducer(state = initialState, action){
    switch(action.type){
        case AUTH_CONSTANTS.SIGNUP_REQUEST:
            return { 
                signIn: true,
                user: action.user
            };
        case AUTH_CONSTANTS.SIGNUP_SUCCESS:
            return {
                signedIn: true,
                user: action.user
            };
        case AUTH_CONSTANTS.SIGNUP_FAILURE:
            return {};

        case AUTH_CONSTANTS.LOGIN_REQUEST:
            return { 
                islogged:false,
                profileId: action.profileId,
            };
        case AUTH_CONSTANTS.LOGIN_SUCCESS:
            return {
                islogged:true,
                profileId: action.profileId,
            };
        case AUTH_CONSTANTS.LOGIN_FAILURE:
            return {
                islogged:false,
                error : action.error
            };

        default:
        return state
    }
}