import { GEO_LOCATION_CONSTANTS } from '../constants/geolocation.constants';

const initialState = [] ;

export default function adsReducer(state = initialState, action){
    switch(action.type){
        case GEO_LOCATION_CONSTANTS.GET_LOCATION_REQUEST:
            return { locationLoaded : false, ...state };
        case GEO_LOCATION_CONSTANTS.GET_LOCATION_SUCCESS:
            return { locationLoaded : true, currentLocation: action.currentLocation };
        case GEO_LOCATION_CONSTANTS.GET_LOCATION_FAILURE:
            return { error : action.error };
        
        case GEO_LOCATION_CONSTANTS.GET_ADDRESS_REQUEST:
            return { addressParsed : false, ...state };
        case GEO_LOCATION_CONSTANTS.GET_ADDRESS_SUCCESS:
            return { ...state, address: action.address,};
        case GEO_LOCATION_CONSTANTS.GET_ADDRESS_FAILURE:
            return { error : action.error };
        
        default:
            return state;
    };
};