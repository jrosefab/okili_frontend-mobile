import { AUTH_CONSTANTS } from "../constants/auth.constants";

const initialState = { tokenIsValid:null, checkTokenIsRunning:false };

export default function checkTokenReducer(state = initialState, action) {
    switch(action.type) {
        case AUTH_CONSTANTS.CHECKTOKEN_REQUEST:
            return {
                tokenIsValid:null,
                checkTokenIsRunning:true
            };
        case AUTH_CONSTANTS.CHECKTOKEN_SUCCESS:
            return {
                tokenIsValid:action.tokenIsValid,
                checkTokenIsRunning:false
            };
        case AUTH_CONSTANTS.CHECKTOKEN_FAILURE:
            return {
                tokenIsValid:action.tokenIsValid,
                checkTokenIsRunning:false
            };
        default:
                return state;
    }
}