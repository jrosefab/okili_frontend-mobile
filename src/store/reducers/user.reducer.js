import { USER_CONSTANTS } from '../constants/user.constants' ;
//import update from 'immutability-helper';

const initialState = {
    userLoaded:false,
};

export default function userReducer(state = initialState, action){
    switch(action.type){
        // handle user data
        case USER_CONSTANTS.GET_USER_REQUEST:
            return { loadUserData: true };
        case USER_CONSTANTS.GET_USER_SUCCESS:
            return { userLoaded : true, user : action.user };
        case USER_CONSTANTS.GET_USER_FAILURE:
            return { userLoaded : false, error : action.error };
            
        /*case USER_CONSTANTS.UPDATE_USER_DATA_REQUEST:
            return update(state, { $merge : action.updateUser });
        case USER_CONSTANTS.UPDATE_USER_DATA_SUCCESS:
            return { ...state, ...action.updateUser };
        case USER_CONSTANTS.UPDATE_USER_DATA_FAILURE:
            return { error : action.error };*/
            
        default:
            return state;
    }
}