import { ADS_CONSTANTS } from '../constants/ads.constants';

const initialState = [] ;

export default function adsReducer(state = initialState, action){
    switch(action.type){
        case ADS_CONSTANTS.GET_ADS_REQUEST:
            return { adsLoaded : false, ...state };
        case ADS_CONSTANTS.GET_ADS_SUCCESS:
            return { adsLoaded : true, recentAds: action.recentAds };
        case ADS_CONSTANTS.GET_ADS_FAILURE:
            return { error : action.error };
        
        case ADS_CONSTANTS.CREATE_AD_REQUEST:
            return { ...state, adCreated : false };
        case ADS_CONSTANTS.CREATE_AD_SUCCESS:
            return { ...state, adCreated : true, newAd: action.newAd };
        case ADS_CONSTANTS.CREATE_AD_FAILURE:
            return {  ...state, error : action.error };

        case ADS_CONSTANTS.GET_MY_ADLIST_REQUEST:
            return { ...state, myAdListLoaded : false };
        case ADS_CONSTANTS.GET_MY_ADLIST_SUCCESS:
            return { ...state, myAdListLoaded : true, myAdList: action.myAdList };
        case ADS_CONSTANTS.GET_MY_ADLIST_FAILURE:
            return {  ...state, error : action.error };

        case ADS_CONSTANTS.GET_SINGLE_AD_REQUEST:
            return { ...state, singleAdLoaded : false };
        case ADS_CONSTANTS.GET_SINGLE_AD_SUCCESS:
            return { ...state, singleAdLoaded : true, singleAd: action.singleAd };
        case ADS_CONSTANTS.GET_SINGLE_AD_FAILURE:
            return {  ...state, error : action.error };

        default:
            return state;
    };
};