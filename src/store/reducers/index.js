import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import adsReducer from '../reducers/ads.reducer';
import userReducer from '../reducers/user.reducer';
import authReducer from '../reducers/auth.reducer';
import geoLocationReducer from '../reducers/geolocation.reducer';

const rootReducer = combineReducers({
    adsReducer,
    geoLocationReducer,
    userReducer,
    authReducer,
    form : formReducer
});

export default rootReducer;