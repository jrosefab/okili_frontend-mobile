import { applyMiddleware, createStore } from 'redux';
import thunkMiddleware from "redux-thunk";
import { createLogger } from 'redux-logger';
import rootReducer from './reducers/index';
import { composeWithDevTools } from 'redux-devtools-extension';
const loggerMidlleware = createLogger();

const store = createStore(
               rootReducer,
               composeWithDevTools(   
                    applyMiddleware(
                        loggerMidlleware,
                        thunkMiddleware
                    )
                )
);

export default store;