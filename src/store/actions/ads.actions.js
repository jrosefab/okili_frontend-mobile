import { getRecentAdsApi, createAdApi,
         getMyAdListApi, getSingleAdApi } from '../../api/ads.api';
import { ADS_CONSTANTS } from '../constants/ads.constants';

export const adsActions = {
    getRecentAdsAction,
    createAdAction,
    getMyAdListAction,
    getSingleAdAction
  };

function getRecentAdsAction(){
    return dispatch => {
      dispatch(request());
      getRecentAdsApi()
        .then(
            recentAds => dispatch(success(recentAds)),
            error => dispatch(failure(error.toString()))
        );
    }
    function request(){ 
        return { type: ADS_CONSTANTS.GET_ADS_REQUEST }; 
    };
    function success(recentAds){ 
        return { type: ADS_CONSTANTS.GET_ADS_SUCCESS, recentAds }; 
    };
    function failure(error){ 
        return { type: ADS_CONSTANTS.GET_ADS_FAILURE, error }; 
    };
};

function createAdAction(newAd){
    return dispatch => {
      dispatch(request());
      createAdApi(newAd)
        .then(
            newAd => dispatch(success(newAd)),
            error => dispatch(failure(error))
        );
    }
    function request(){ 
        return { type: ADS_CONSTANTS.CREATE_AD_REQUEST }; 
    };
    function success(newAd){ 
        return { type: ADS_CONSTANTS.CREATE_AD_SUCCESS, newAd }; 
    };
    function failure(error){ 
        return { type: ADS_CONSTANTS.CREATE_AD_FAILURE, error }; 
    };
};

function getMyAdListAction(user){
    return dispatch => {
      dispatch(request());
      getMyAdListApi(user)
        .then(
            myAdList => dispatch(success(myAdList)),
            error => dispatch(failure(error.toString()))
        );
    }
    function request(){ 
        return { type: ADS_CONSTANTS.GET_MY_ADLIST_REQUEST }; 
    };
    function success(myAdList){ 
        return { type: ADS_CONSTANTS.GET_MY_ADLIST_SUCCESS, myAdList }; 
    };
    function failure(error){ 
        return { type: ADS_CONSTANTS.GET_MY_ADLIST_FAILURE, error }; 
    };
};

function getSingleAdAction(adId){
    return dispatch => {
      dispatch(request());
      getSingleAdApi(adId)
        .then(
            singleAd => dispatch(success(singleAd)),
            error => dispatch(failure(error.toString()))
        );
    }
    function request(){ 
        return { type: ADS_CONSTANTS.GET_SINGLE_AD_REQUEST }; 
    };
    function success(singleAd){ 
        return { type: ADS_CONSTANTS.GET_SINGLE_AD_SUCCESS, singleAd }; 
    };
    function failure(error){ 
        return { type: ADS_CONSTANTS.GET_SINGLE_AD_FAILURE, error }; 
    };
};