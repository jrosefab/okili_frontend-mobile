import { getCurrentLocationApi, getAddressFromCoordinatesApi } from '../../api/geolocation.api';
import { GEO_LOCATION_CONSTANTS } from '../constants/geolocation.constants';

export const getCurrentLocationAction = () =>{
    return dispatch => {
      dispatch(request());
      getCurrentLocationApi()
        .then(
            currentLocation => dispatch(success(currentLocation)),
            error => dispatch(failure(error))
        );
    }
    function request(){ 
        return { type: GEO_LOCATION_CONSTANTS.GET_LOCATION_REQUEST }; 
    };
    function success(currentLocation){ 
        return { type: GEO_LOCATION_CONSTANTS.GET_LOCATION_SUCCESS, currentLocation }; 
    };
    function failure(error){ 
        return { type: GEO_LOCATION_CONSTANTS.GET_LOCATION_FAILURE, error }; 
    };
};

export const getAddressFromCoordinatesAction = (latitude, longitude) => {
    return dispatch => {
      dispatch(request());
      getAddressFromCoordinatesApi(latitude, longitude)
        .then( 
            address => dispatch(success(address)),
            error => dispatch(failure(error))
        );
    };
    function request(){ 
        return { type: GEO_LOCATION_CONSTANTS.GET_ADDRESS_REQUEST }; 
    };
    function success(address){ 
        return { type: GEO_LOCATION_CONSTANTS.GET_ADDRESS_SUCCESS, address }; 
    };
    function failure(error){ 
        return { type: GEO_LOCATION_CONSTANTS.GET_ADDRESS_FAILURE, error }; 
    };
};