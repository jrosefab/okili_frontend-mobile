import { signupApi, loginApi  } from '../../api/auth.api'
import { AUTH_CONSTANTS } from '../constants/auth.constants';

export const authActions = {
  signupAction,
  loginAction
};

function signupAction(user){
  return dispatch => {
    dispatch(request (user));
    signupApi(user)
    .then(user => {
            dispatch(success(user));
            this.navigation.navigate('login')
          },
          error => {
            dispatch(failure(error.toString()));
          }
    );
  };

  function request(user){ return { type: AUTH_CONSTANTS.SIGNUP_REQUEST, user }}
  function success(user){ return { type: AUTH_CONSTANTS.SIGNUP_SUCCESS, user }}
  function failure(error){return { type: AUTH_CONSTANTS.SIGNUP_FAILURE, error }}
}

function loginAction(user) {
  return dispatch => {
    dispatch(request(user));
    loginApi(user)
      .then(
          user => { dispatch(success(user));
                    this.navigation.goBack(null)
                  },
          error => dispatch(failure(error.toString()))
      );
  };

  function request(user){ 
    return {type: AUTH_CONSTANTS.LOGIN_REQUEST, user}
  }
  function success(profileId){ 
    return {type: AUTH_CONSTANTS.LOGIN_SUCCESS, profileId }
  }
  function failure(error){ 
    return {type: AUTH_CONSTANTS.LOGIN_FAILURE, error}
  }
}