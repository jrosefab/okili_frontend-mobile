import { getUserApi } from '../../api/user.api'
import { USER_CONSTANTS } from '../constants/user.constants';

export const getUserAction = (id) =>{
    return dispatch => {
      dispatch(request());
      getUserApi(id)
        .then(
          user => dispatch(success(user)),
          error => dispatch(failure(error.toString()))
        );
    };
    function request(){ return { type: USER_CONSTANTS.GET_USER_REQUEST }; };
    function success(user){ return { type: USER_CONSTANTS.GET_USER_SUCCESS, user }; };
    function failure(error){ return { type: USER_CONSTANTS.GET_USER_FAILURE, error }; };
};