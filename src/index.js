import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import RootStackNavigator from './routes/RootStackNavigator';

const MainApp = () => {
  return (
    <NavigationContainer>
        <RootStackNavigator/>
    </NavigationContainer>
  );
};

export default MainApp;