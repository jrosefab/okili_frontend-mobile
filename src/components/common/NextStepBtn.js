import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export const NextStepBtn = ( props ) => {
    const  { color, style, onPress} = props
    return (
        <View style={style}>
            <View>
                <TouchableOpacity onPress={onPress}>
                    <FontAwesome5 style={styles.icon} name="arrow-right" size={20} />
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    icon : {
        width : 50,
        backgroundColor : '#76C14B',
        borderRadius : 150,
        paddingHorizontal : 16,
        paddingVertical : 14,
        color:'#fff',
    }
});