import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import BouncingPreloader from 'react-native-bouncing-preloader';
import Lemon from '../../assets/illustrations/lemon.png';
import Salt from '../../assets/illustrations/salt.png';
import Banana from '../../assets/illustrations/banana.png';
import Lighter from '../../assets/illustrations/lighter.png';
import Scotch from '../../assets/illustrations/scotch.png';
import Box from '../../assets/illustrations/box.png';

const illustrations = [
    Lemon,               
    Salt,
    Banana,
    Lighter,
    Scotch,
    Box
]

export const Loading = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.items}>
                <BouncingPreloader
                    icons={illustrations}
                    leftRotation="-680deg"
                    rightRotation="360deg"
                    leftDistance={-100}
                    rightDistance={-150}
                    speed={1200} 
                />
            </View>
            <View>
                <Text style={{ alignSelf : 'center'}}>
                    {props.children}
                </Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container : {
        flexGrow : 1,
        marginVertical : 200,
        width : 60,
        height: 250,
    },
    items : {
        height: '100%',
        alignItems: "center",
        justifyContent: "center",
        alignContent : "center",
        backgroundColor: "#fff",
    }
});