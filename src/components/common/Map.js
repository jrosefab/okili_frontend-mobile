import React from 'react';
import MapView, { Marker } from 'react-native-maps';
import { View, ScrollView, StyleSheet } from 'react-native';

export const Map = (props) => {
    const {onMapReady, region, coordinateMark } = props;
    return (
        <ScrollView>
            <View style={styles.mapContainer}>
                <MapView style={styles.map}
                        followsUserLocation={true}
                        onMapReady={onMapReady}
                        region={region}
                        pitchEnabled={false} 
                        rotateEnabled={false} 
                        zoomEnabled={false} 
                        scrollEnabled={false}
                >
                    <Marker.Animated style={{ position: 'absolute', top: 100, left: 50 }}
                                    coordinate={coordinateMark}
                                    title={"Lieu de retrait"}
                                    description={"Domicile"}
                    />
                </MapView>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    mapContainer : {
        height: 200,
        width: 350,
        alignSelf : "center",
        marginTop : 10
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    }
});