import React from 'react';
import { View, Text, StyleSheet, TouchableWithoutFeedback } from 'react-native';

export const TabBar = ( props ) => {
    const  { color, label, size } = props
    return (
        <View style={[styles.button, color ]}>
            <View size={size}>{ props.children }</View>
            <Text style={styles.text}>{ label }</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    button: {
      alignItems: 'center',
    },
    text : {
        fontSize: 12
    }
});