import React from 'react';
import StepIndicator from 'react-native-step-indicator';
console.disableYellowBox = true;

const customStyles = {
  stepIndicatorSize: 22,
  currentStepIndicatorSize: 22,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#7ED6DF',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#7ED6DF',
  stepStrokeUnFinishedColor: '#dedede',
  separatorFinishedColor: '#7ED6DF',
  separatorUnFinishedColor: '#dedede',
  stepIndicatorFinishedColor: '#7ED6DF',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 0,
  currentStepIndicatorLabelFontSize: 0,
  stepIndicatorLabelCurrentColor: 'transparent',
  stepIndicatorLabelFinishedColor: 'transparent',
  stepIndicatorLabelUnFinishedColor: 'transparent',
  labelColor: '#999999',
  labelSize: 10,
  labelFontFamily: 'OpenSans-Italic',
  currentStepLabelColor: '#7ED6DF'
}

export const StepCustom = (props) => {
    const { currentPosition, stepCount, labels } = props

    return (
        <StepIndicator
            stepCount={stepCount} 
            customStyles={customStyles}
            currentPosition={currentPosition}
            labels={labels}
        />
    );
}