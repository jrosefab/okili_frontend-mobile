import React from 'react';
import { TouchableWithoutFeedback, 
         StyleSheet, Image,
         Text, View } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

export const AdCard = (props) => {
    return (
        <TouchableWithoutFeedback onPress={props.getAd}>
            <View style={styles.cardWrapper}>
                <View style={styles.cardContainer}>
                    <View style={{ position : 'relative' }}>
                        <Image
                            style={styles.adPicture}
                            source={{ uri: props.picture }}
                        />
                        <View style={styles.kindPictoContainer}>
                            {props.isShopping ? 
                                <View style={[styles.kindPicto, 
                                             { backgroundColor : "#76C14B" }]} 
                                      onPress={props.addToBasket}>
                                    <FontAwesome5 style={{ paddingHorizontal : 10, 
                                                            paddingVertical : 5, 
                                                            color : '#fff' }} 
                                                  name="shopping-basket" 
                                                  size={15} 
                                    />
                                </View>
                            :
                                <View style={[styles.kindPicto, { backgroundColor : "#F19CB8"}]} onPress={props.sendProposal}>
                                    <FontAwesome5 style={{ paddingHorizontal : 10, 
                                                            paddingVertical : 5, 
                                                            color : '#fff' }} 
                                                  name="hand-holding-heart"
                                                  size={15} 
                                    />
                                </View>
                            }
                        </View>
                        <View style={styles.adPictoInfos}>
                            <View style={styles.picto}>
                                <Entypo style={styles.icon} name="location-pin" size={15} />                                        
                                <Text style={[styles.icon, { fontSize : 8 }]}>
                                    100m{props.location}
                                </Text>
                            </View>
                            <View style={styles.picto}>
                                <FontAwesome5 style={styles.icon} name={props.conditionType} size={15} />
                                <Text style={[styles.icon, { fontSize : 8 }]}>
                                    {props.value}
                                </Text>
                            </View>
                            <View style={styles.picto}>
                                <MaterialIcons style={styles.icon} name="timer" size={15} />
                                <Text style={[styles.icon, { fontSize : 8 }]}>
                                    {props.urgency}
                                </Text>
                            </View>
                            <View style={styles.picto}>
                                <Ionicons style={styles.icon} name="ios-pricetag" size={15} />
                                <Text style={[styles.icon, { fontSize : 8 }]}>
                                    {props.price}
                                </Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.ItemTitle}>
                        <Text style={{ fontSize : 16 }}>{props.title}</Text>
                        <Text style={{ fontSize : 8 }}>{props.dateTime}</Text>
                    </View>
                    <View style={styles.ItemDescription}>
                        <Text style={{ fontSize : 10, lineHeight : 15 }}>{props.description}</Text>
                    </View>
                    <View style={styles.adActionContainer}>
                        {props.isShopping ?
                            <View style={styles.adAction} onPress={props.addToBasket}>
                                <Text style={[styles.adBtn, { color : '#3c8710' , borderColor : '#76C14B' }]}>
                                    J'en ai besoin
                                </Text>
                            </View>
                        :
                            <View style={styles.adAction} onPress={props.sendProposal}>
                                <Text style={[styles.adBtn, {color : '#e34277' , borderColor : '#F19CB8' }]}>
                                    J'ai ce qu'il faut
                                </Text>
                            </View>
                        }
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    cardWrapper: {
        display: 'flex',
        width : '50%',
        padding : 5,
        flexDirection : 'column',
        justifyContent : 'space-between',
    },
    cardContainer: {
        backgroundColor : '#fff',
        margin : 4,
        borderRadius : 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 10,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 8,
    },
    ItemTitle : {
        display : 'flex', 
        flex: 1,
        flexDirection : 'column', 
        alignItems: 'baseline',
        marginVertical: 5,
        marginLeft : 10
    },
    ItemDescription : {
        marginHorizontal : 4,
        padding : 5,
    },
    adPicture : {
        width: "100%",
        display : 'flex',
        borderTopRightRadius : 20,
        borderTopLeftRadius : 20,
        justifyContent : 'center',
        marginHorizontal : 0,
        marginVertical : 0,
        height : 160,
        position : 'relative'
    },
    adPictoInfos: {
        display : 'flex',
        flexDirection : 'row', 
        justifyContent : 'space-between',
        marginHorizontal : 0.5,
        marginVertical : 5,
        position : 'absolute',
        bottom : 0,
        opacity : 0.8
    },
    kindPictoContainer : {
        position : 'absolute',
        top : 0,
        width : '100%',
        color: '#FFFFFF',
        alignItems : 'center',
        justifyContent : 'center'
    },
    kindPicto : {
        color: '#FFFFFF',
        alignItems : 'center',
        borderBottomRightRadius : 18,
        width : 40,
        borderBottomLeftRadius : 18,
        justifyContent : 'center'
    },
    picto : {
        display : 'flex',
        justifyContent : 'center',
        backgroundColor : '#fff',
        alignItems : 'center',
        width : 38,
        marginHorizontal: 3,
        marginVertical: 2,
        borderRadius : 8,
        padding: 5,
    },
    icon : {
        color : "#4B4B4B"
    },
    adActionContainer: {
        display : 'flex',
        flexDirection : 'row', 
        color: '#FFFFFF',
        width : '100%',
        alignItems : 'center',
        justifyContent : 'center',
        padding : 6
    },
    adAction: {
        display : 'flex',
        alignContent : 'center',
        flexDirection : 'row', 
        alignItems : 'center',
        justifyContent : 'center',
        marginVertical : 5,
        marginHorizontal : 5,
    },
    adBtn: {
        fontSize : 12, 
        paddingHorizontal : 25, 
        paddingVertical : 8, 
        borderRadius : 30,
        borderWidth : 2
    },
    pictoAction : {
        marginRight : 5,
        padding : 6,
        borderRadius : 14,
        width : 35,
        textAlign : 'center',
        color: '#fff'
    }
});
