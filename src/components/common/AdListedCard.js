import React from 'react';
import { TouchableWithoutFeedback, 
         StyleSheet, Image,
         Text, View } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

export const AdListedCard = (props) => {
    return (
        <TouchableWithoutFeedback onPress={props.getAd}>
            <View style={styles.cardWrapper}>
                <View style={styles.cardContainer}>
                    <View style={styles.cardHeader}>
                        <Image
                                style={styles.adPicture}
                                source={{ uri: props.picture }}
                        />
                        <View style={styles.ItemTitle}>
                            <Text style={{ fontSize : 16 }}>{props.title}</Text>
                            <Text style={{ fontSize : 8 }}>{props.dateTime}</Text>
                            <View style={styles.ItemDescription}>
                                <Text style={{ fontSize : 12 }}>{props.description}</Text>
                            </View>       
                        </View>
                    </View>
                    <View style={styles.adActionContainer}>
                        {props.haveProposals ? 
                            <View style={styles.adAction} onPress={props.addToBasket}>
                                <Text style={styles.adBtn}>
                                    Voir les propositions
                                </Text>
                            </View>
                        :
                            <View style={styles.adAction} onPress={props.sendProposal}>
                                <Text style={styles.adBtn}>
                                    En attente de propositions
                                </Text>
                            </View>
                        }
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    cardWrapper: {
        display: 'flex',
        width : '100%',
        padding : 5,
        flexDirection : 'column',
        justifyContent : 'space-between',
    },
    cardHeader: {
        display :"flex", 
        flexDirection : "row", 
        paddingHorizontal : 15, 
        paddingTop : 10
    },
    cardContainer : {
        backgroundColor : '#fff',
        margin : 4,
        borderRadius : 20,
        shadowColor: "#000",
        shadowOffset: {
            width: 10,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 8,
    },
    ItemTitle : {
        display : 'flex', 
        flex: 1,
        flexDirection : 'column', 
        alignItems: 'baseline',
        marginLeft : 10
    },
    ItemDescription : {
        marginTop : 15,
    },
    adPicture : {
        width: 60,
        height : 60,
        display : 'flex',
        borderRadius : 15,
        justifyContent : 'center',
        marginHorizontal : 0,
        marginVertical : 0,
        position : 'relative'
    },
    adPictoInfos: {
        display : 'flex',
        flexDirection : 'row', 
        justifyContent : 'space-between',
        marginHorizontal : 0.5,
        marginVertical : 5,
        position : 'absolute',
        bottom : 0,
        opacity : 0.8
    },
    kindPictoContainer : {
        position : 'absolute',
        top : 0,
        width : '100%',
        color: '#FFFFFF',
        alignItems : 'center',
        justifyContent : 'center'
    },
    kindPicto : {
        color: '#FFFFFF',
        alignItems : 'center',
        borderBottomRightRadius : 18,
        width : 40,
        borderBottomLeftRadius : 18,
        justifyContent : 'center'
    },
    icon : {
        color : "#4B4B4B"
    },
    adActionContainer: {
        display : 'flex',
        flexDirection : 'row', 
        width : '100%',
        alignItems : 'center',
        justifyContent : 'center',
        padding : 6
    },
    adAction: {
        display : 'flex',
        alignContent : 'center',
        flexDirection : 'row', 
        alignItems : 'center',
        justifyContent : 'center',
        borderColor : '#79CED6',
        marginVertical : 5,
        marginHorizontal : 5,
    },
    adBtn: {
        fontSize : 12, 
        color : '#4B4B4B',
        paddingHorizontal : 25, 
        paddingVertical : 8, 
        borderRadius : 30,
        borderWidth : 2,
        borderColor : '#7FCBE2'
    },
});
