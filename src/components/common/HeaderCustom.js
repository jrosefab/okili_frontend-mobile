import React from 'react';
import { StyleSheet, Text, View} from 'react-native';

export const HeaderCustom = (props) => {
    return (
        <View style={styles.container}>
            <Text style={styles.currentStep}>
                 {props.currentStep}
            </Text> 
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection : 'row',
        alignContent : 'center',
        justifyContent : 'center'
    },
    currentStep : {
        fontSize : 18,
        textAlign:"center",
        fontWeight : 'bold',
        alignSelf :'center',
        color : '#4B4B4B'
    },
    icon : {
        color : '#B2BEC3' 
    },
    text: {
        fontSize: 16,
        color: '#FFFFFF',
    },
});
