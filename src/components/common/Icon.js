import React from 'react';
import { StyleSheet, View, Text } from 'react-native';

export const Icon = ( props ) => {
    const  { color, label, size, onPress } = props
    return (
        <View style={[styles.button, color ]}>
            <View size={size}>{ props.children }</View>
            <Text style={styles.text}>{ label }</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    button: {
      alignItems: 'center',
    },
    text : {
        fontSize: 10
    }
});