import React from 'react';
import { TouchableWithoutFeedback, StyleSheet } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

export const EllipseOptions = (props) => {
    const { onPress } = props;
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <SimpleLineIcons style={styles.icon} name="options-vertical" size={20} />
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    icon : {
        color : '#B2BEC3' ,
        margin : 20,
    },
});
