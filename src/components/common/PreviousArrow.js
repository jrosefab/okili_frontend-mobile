import React from 'react';
import { TouchableWithoutFeedback, StyleSheet, View } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export const PreviousArrow = (props) => {
    const { onPress, style } = props;
    return (
        <TouchableWithoutFeedback onPress={onPress}>
            <View style={style}>
                <FontAwesome5 style={styles.icon} name="arrow-left" size={20} />
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    icon : {
        margin : 20,
        color : '#7ED6DF'
    },
});