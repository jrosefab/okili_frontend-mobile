import React from 'react';
import { View, ActivityIndicator, 
         Text, StyleSheet } from 'react-native';

export const CircleLoader = () => {
    return (
        <View style={styles.circleLoader}>
            <ActivityIndicator size="large" color="#7ED6DF"/>
            <Text style={{ alignSelf : 'center'}}>Veuillez activer votre localisation</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    circleLoader : {
        flex: 1,
        justifyContent: "center",
        flexDirection: "column",
        justifyContent: "center",
        padding: 10
    }
});
