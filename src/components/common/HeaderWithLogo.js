import React from 'react';
import { TouchableWithoutFeedback, StyleSheet, 
         Image, View } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Logo from '../../assets/logo.png';

export const HeaderWithLogo = (props) => {
    const { onPress } = props;
    return (
        <View style={styles.container}>
            <Entypo style={styles.icon} name="location-pin" size={25} />                                   
            <TouchableWithoutFeedback onPress={onPress}>
                <Image
                    style={styles.tinyLogo}
                    source={Logo}
                />
            </TouchableWithoutFeedback>
            <FontAwesome5 style={styles.icon} name="piggy-bank" size={25} />                                 
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection : 'row',
        justifyContent : 'space-between',
        alignItems : 'center',
    },
    tinyLogo : {
        width: 70,
        height : 50
    },
    icon : {
        color : '#B2BEC3'
    },
    text: {
        fontSize: 16,
        color: '#FFFFFF',
    },
});
