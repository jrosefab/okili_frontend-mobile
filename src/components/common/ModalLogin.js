import React from 'react';
import MapView, { Marker } from 'react-native-maps';
import { View, Modal, StyleSheet,
    TouchableWithoutFeedback, Text,TouchableOpacity} from 'react-native';

export const ModalLogin = (props) => {
    const { signup, onPress, login, 
            visible, onRequestClose } = props;

    return (
        <Modal
            animationType="fade"
            transparent={true}
            visible={visible}
            onRequestClose={onRequestClose}
        >
            <TouchableWithoutFeedback onPress={onPress}>
                <View style={styles.centeredView}>
                    <View style={styles.authContainer}>
                        <View style={{ marginVertical : 10 }}>
                            <Text style={[styles.highTitle, {alignSelf :'center'}]}>
                                J'ai déjà un compte
                            </Text>
                            <TouchableOpacity onPress={login}>
                                <View style={styles.auth_login}>
                                    <Text style={styles.auth_text}>Se connecter</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginVertical : 10 }}>
                            <Text style={[styles.highTitle, {alignSelf :'center'}]}>
                                Je n'ai pas de compte
                            </Text>                                
                            <TouchableOpacity onPress={signup}>
                                <View style={styles.auth_sign}>
                                    <Text style={[styles.auth_text, { color : '#7FCBE2'}]}>S'enregistrer</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </Modal>
    );
};

const styles = StyleSheet.create({
    highTitle : {
        marginVertical : 16,
        fontWeight : 'bold',
        fontSize : 18,
        color : '#4B4B4B',
    },
    centeredView: {
        flex : 1,
        justifyContent :'center',
        marginBottom : 0,
        backgroundColor : "rgba(0, 0, 0, 0.72)"
      },
      authContainer : {
        backgroundColor : "#fff",
        alignSelf : 'center',
        width : 350,
        margin : 5,
        padding : 20,
        borderRadius : 30,
    },
    auth_login : {
        backgroundColor : '#7FCBE2',
        padding : 20,
        borderRadius : 300,
    },
    auth_sign : {
        borderWidth : 1, 
        borderColor : '#7FCBE2',
        padding : 20,
        borderRadius : 300,
    },
    auth_text: {
        textAlign : 'center',
        fontWeight : 'bold',
        color : '#fff'
      },
})