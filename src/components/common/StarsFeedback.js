import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

export const StarsFeedback = (props) => {
    const { count } = props
    const [stars, setStars] = useState({
            first  : false,
            second : false,
            third  : false,
            fourth : false,
            fifth  : false,
    }) 

    useEffect(() => {
        const sliced = Object.keys(stars).slice(0, count).reduce((result, key) => {
                    result[key] = stars[key];
                    result[key] = true
                    return result;
        }, {});
        
        setStars(prevState => ({
            ...prevState, ...sliced
        }))
    }, [])

    return (
        <View style={styles.feedback_container}>
            <View style={styles.row_centered}>
                {Object.entries(stars).map((key) => (
                    <FontAwesome 
                        style={[styles.star, 
                                key[1] && styles.yellowStar 
                            ]}
                        name="star"             
                        key={key} 
                        size={16} 
                    />
                ))}
            </View>
            <Text style={styles.text}>{count}/5</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    feedback_container : {
      alignItems : 'center'
    },
    row_centered : {
        display : 'flex' , 
        flexDirection : 'row', 
        alignItems : 'center',
        alignContent: 'center'
    },
    star : {
        padding : 2,
        color : '#CECCC6'
    },
    yellowStar : {
        color : '#FBC636'
    },
    text : {
        fontSize : 16,
        color : '#A5A5A5'
    }
})