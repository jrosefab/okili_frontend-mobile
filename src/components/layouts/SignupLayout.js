import React from 'react';
import {TouchableWithoutFeedback, 
        StyleSheet, Text, View } from 'react-native';

export const SignupLayout = (props) => {
  return (
    <TouchableWithoutFeedback>
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text style={{ marginTop: 50, fontSize: 25 }}>LoginLayout</Text>
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          {props.children}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
});