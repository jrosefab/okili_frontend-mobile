import React from 'react';
import {TouchableWithoutFeedback,
        Text, View, StyleSheet } from 'react-native';

export const ProposalsLayout = (props) => {
  return (
    <TouchableWithoutFeedback>
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text style={{ marginTop: 50, fontSize: 25 }}>ProposalsLayout</Text>
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          {props.children}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
});