import React, {useState} from 'react';
import { Text, StyleSheet, Image, 
         TouchableOpacity, View, ScrollView, } from 'react-native';
import { TabBar, PreviousArrow, Map, StarsFeedback } from '../../components';
import Entypo from 'react-native-vector-icons/Entypo'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

export const SingleAdLayout = (props) => {

    const renderImgContainer = () => (
        <>
            <Image
                style={styles.image}
                source={{ uri: props.picture }}
            />
            <View style={styles.adActionContainer}>
                {props.isShopping ? 
                 <TouchableOpacity onPress={props.addToBasket}>
                    <View style={[styles.adAction, styles.row_centered,{ backgroundColor : '#76C14B' }]} 
                            onPress={props.isMine ? props.getPropsals : props.addToBasket}>
                        <FontAwesome5 style={{ paddingRight : 20, 
                                                paddingVertical : 5, 
                                                color : '#fff' }} 
                                        name="shopping-basket" 
                                        size={15} 
                        />
                        <Text style={[styles.adBtn, { borderColor : '#76C14B' }]}>
                            {props.isMine ? "Voir mes propositions" : "J'en ai besoin"}
                        </Text>
                    </View>
                 </TouchableOpacity>
                :
                <TouchableOpacity onPress={props.onProposal}>
                    <View style={[styles.adAction, 
                                  styles.row_centered, 
                                { backgroundColor : '#F19CB8' }]} 
                    >
                        <FontAwesome5 style={{ paddingRight : 20, 
                                                paddingVertical : 5, 
                                                color : '#fff' }} 
                                        name="hand-holding-heart"
                                        size={15} 
                            />
                            <Text style={[styles.adBtn, { borderColor : '#F19CB8' }]}>
                            {props.isMine ? "Voir mes propositions" : "J'ai ce qu'il faut"}
                        </Text>
                    </View>
                </TouchableOpacity>
                }
            </View>
        </>
    )

    const renderAdOptions = () => (
        <>
            <View style={[styles.ad_option__item, styles.row_centered ]}>
                <TouchableWithoutFeedback  style={styles.row_centered}>
                    <MaterialIcons style={styles.icon} name="timer" size={20} />
                    <Text style={{ fontSize : 12, padding : 10 }}>{props.dateTime}</Text>
                </TouchableWithoutFeedback>
            </View>
            <View style={[styles.ad_option__item, styles.row_centered ]}>
                <TouchableWithoutFeedback  style={styles.row_centered} onPress={props.onShare}>
                    <Entypo style={styles.icon} name="share" size={20} />
                    <Text style={{ fontSize : 12, padding : 10 }}>Partager</Text>
                </TouchableWithoutFeedback>
            </View>
            <View style={[styles.ad_option__item, styles.row_centered, { borderRightWidth : 0 } ]}>
                <TouchableWithoutFeedback  style={styles.row_centered} onPress={props.onSignal}>
                    <Feather style={styles.icon} name="alert-triangle" size={20} />
                    <Text style={{ fontSize : 12, padding : 10 }}>Signaler</Text>
                </TouchableWithoutFeedback>
            </View>
        </>
  )

  const renderDescription = () => (
        <View style={{paddingHorizontal : 20 }}>
            <Text style={styles.title}>{props.adTtile}</Text>
            <Text style={styles.content}>{props.description}</Text>
        </View>
  )

  const renderBuyingCondition = () => (
    <>
        <View style={{paddingHorizontal : 20, paddingVertical : 8 }}>
            <Text style={styles.title}>Prix conséillé</Text>
            <Text style={styles.content}>{props.price}</Text>
        </View>
        <View style={{paddingHorizontal : 20, paddingVertical : 8 }}>
            <Text style={styles.title}>Etat</Text>
            {props.condition}
        </View>
        <View style={{paddingHorizontal : 20, paddingVertical : 8 }}>
            <Text style={styles.title}>Mode de livraison</Text>
            {props.delivery}
        </View>
    </>
  )

  const renderMap = () => (
    <Map region={{ 
        latitude: props.latitude,
        longitude: props.longitude, 
        latitudeDelta: 0.009,
        longitudeDelta: 0.009,
        }}
        coordinateMark={{ 
            latitude: props.latitude,
            longitude: props.longitude 
        }}
    />
  )

  const renderUser = () => (
    <View>
        <View style={[styles.row_centered, { justifyContent : 'space-evenly' }]}>
            <Image style={styles.creator_pic} source={{ uri : props.creatorPicture }} />
            <View>
                <Text style={styles.creator_link}>{props.creator}</Text>
                <View><StarsFeedback count={props.stars} /></View>
            </View>
        </View>
        <View style={[styles.row_centered, {justifyContent : 'space-around'}]}>
            <View style={[styles.row_centered, 
                            styles.isVerified,
                            props.isVerified ? 
                            { borderColor : "#87D15C"}
                            :
                            { borderColor : "#F19CB8"}
                        ]} 
                >
                <FontAwesome style={styles.icon} name="mobile-phone" size={20} />
                <Text style={{ fontSize : 16, padding : 10 }}>
                        Téléphone {props.isVerified ? null : "non" } vérifié 
                </Text>
            </View>
        </View>
    </View>
)

const renderPropsals = () => (
    <View>
        <View style={[styles.row_centered, { justifyContent : 'space-evenly' }]}>
            <Image style={styles.creator_pic} source={{ uri : props.creatorPicture }} />
            <View>
                <Text style={styles.creator_link}>{props.creator}</Text>
                <View><StarsFeedback count={props.stars} /></View>
            </View>
        </View>
        <View style={[styles.row_centered, {justifyContent : 'space-around'}]}>
            <View style={[styles.row_centered, 
                            styles.isVerified,
                            props.isVerified ? 
                            { borderColor : "#87D15C"}
                            :
                            { borderColor : "#F19CB8"}
                        ]} 
                >
                <FontAwesome style={styles.icon} name="mobile-phone" size={20} />
                <Text style={{ fontSize : 16, padding : 10 }}>
                        Téléphone {props.isVerified ? null : "non" } vérifié 
                </Text>
            </View>
        </View>
    </View>
)

  return (
    <View style={styles.container}> 
        <ScrollView>
            <View style={styles.image_container}>
                <PreviousArrow 
                    style={{ position : 'absolute', top : 0, zIndex: 99 }}
                    onPress={() => props.navigation.goBack(null)} 
                    title="Back" {...props}
                />
                {renderImgContainer()}
            </View>
            <View style={styles.ad_option}>
                {renderAdOptions()}
            </View>
            <View style={styles.inner}>
                <View style={styles.ad_infos_container}>
                    <Text style={styles.highTitle}>Description</Text>
                    {renderDescription()}
                </View>
                <View style={styles.ad_infos_container}>
                    <Text style={styles.highTitle}>Conditions d'achat</Text>
                    {renderBuyingCondition()}
                </View>
                <View style={styles.ad_infos_container}>
                    <Text style={styles.highTitle}>Localisation</Text>
                    {renderMap()}
                </View>
                <View style={styles.ad_infos_container}>
                    <Text style={styles.highTitle}>Utilisateur</Text>
                    {renderUser()}
                </View>
                <View style={{ marginBottom : 20 }}>
                    { props.haveProposals ? 
                        <>
                            {props.isMine ? 
                                <Text style={styles.highTitle}>
                                    Vous avez {props.haveProposals}
                                </Text>
                            : 
                                <Text style={styles.highTitle}> 
                                    Il y'a {props.haveProposals} propositions
                                </Text>
                            }
                            {renderPropsals()}
                        </>
                        
                    : 
                    <Text style={styles.highTitle}>Aucune propositions pour le moment</Text>
                    }
                </View>
        </View>
        </ScrollView>
        <View style={styles.bottomBar}>
            <TouchableOpacity onPress={() => props.navigation.goBack(null)} >
                <TabBar label="Retour à l'accueil">
                    <Entypo name="home" size={30} color={"#B2BEC3"} />
                </TabBar>
            </TouchableOpacity>
            {props.isMine ?
            <>
                <TouchableOpacity onPress={props.onPropose}>
                    <TabBar label="Propositions reçu">
                        <FontAwesome5 name="hands-helping" size={30} color={"#B2BEC3"} />
                    </TabBar>
                </TouchableOpacity>
                <TouchableOpacity onPress={props.onPropose}>
                    <TabBar label="Modifier">
                        <Entypo name="pencil" size={30} color={"#B2BEC3"} />
                    </TabBar>
                </TouchableOpacity>
                <TouchableOpacity onPress={props.onPropose}>
                    <TabBar label="Supprimer l'annonce">
                        <FontAwesome name="trash" size={30} color="#FF6A6A" />
                    </TabBar>
                </TouchableOpacity>
            </>
            :
            <TouchableOpacity onPress={props.onProposal}>
                <TabBar label="J'ai ce qu'il faut">
                    <FontAwesome5 name="hand-holding-heart" size={30} color={"#B2BEC3"} />
                </TabBar>
            </TouchableOpacity>
            }

        </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        display : 'flex',
        backgroundColor : '#fff',
        position : 'relative',
        paddingBottom : 70,
    },
    image_container : {
        position : 'relative',
        zIndex : 20
    },
    image : {
        width: "100%",
        height : 250,
        display : 'flex',
        marginHorizontal : 0,
        marginVertical : 0,
        position : 'relative'
    },
    inner : {
        paddingHorizontal : 20
    },
    highTitle : {
        marginVertical : 10,
        fontWeight : 'bold',
        fontSize : 18,
        color : '#4B4B4B',
    },
    ad_infos_container : {
        borderBottomWidth : 1,
        borderColor : '#D8D8D8',
        paddingBottom : 20
    },  
    title : {
        fontSize : 16,
        fontWeight : 'bold'
    },
    content : {
        fontSize : 16,
        opacity : 0.6,
    },
    bottomBar : {
        display : 'flex',
        flexDirection : 'row',
        borderTopWidth : 1,
        borderColor : "#B2BEC3",
        justifyContent: 'space-around',
        alignContent : 'center',
        alignItems : 'center',
        height : 80
    },
    adActionContainer : {
        display : 'flex',
        flexDirection : 'row', 
        color: '#FFFFFF',
        width : '100%',
        alignItems : 'center',
        justifyContent : 'center',
        position : 'absolute',
        top : '40%',
        padding : 6,
    },
    adAction : {
        paddingHorizontal : 30,
        borderRadius : 30,
        paddingVertical : 10,
        justifyContent : 'space-around',
        marginVertical : 5,
        marginHorizontal : 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 10,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 8,
    },
    adBtn : {
        color : "#fff"
    },
    ad_option : {
        display : 'flex',
        flexDirection : 'row',
        paddingVertical : 10,
        opacity : 0.4
    },
    ad_option__item : {
        borderRightWidth : 1,
        height : 20,
        flexBasis : '33%',
        flexGrow : 1
    },
    icon : {
        paddingLeft : 20
    },
    row_centered : {
        display : 'flex' , 
        flexDirection : 'row', 
        alignItems : 'center',
        alignContent: 'center'
    },
    creator_link : {
        textAlign : 'center',
        color : '#7FCBE2',
        fontWeight : 'bold',
        fontSize : 16,
        textTransform : 'capitalize'
    },
    creator_pic : {
        height : 50,
        width : 50,
        borderRadius : 100

    },
    star : {
        padding : 2,
    },
    isVerified : {
        borderWidth : 1,
        borderRadius : 60,
        marginVertical : 10,
        paddingHorizontal : 16
    }
})