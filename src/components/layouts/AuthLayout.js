import React from 'react';
import { TouchableWithoutFeedback, 
         StyleSheet, View, Keyboard, 
         KeyboardAvoidingView, ScrollView,
         Image, Text, TouchableOpacity } from 'react-native';
import Logo from '../../assets/logo.png';
import Facebook from '../../assets/facebook.png';
import Gmail from '../../assets/gmail.png';


export const AuthLayout = (props) => {
  const { onSubmit, authMode, isScrollable, 
          authLink, authPage, authMethodText } = props
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <KeyboardAvoidingView style={{ flex : 1 }}
                                behavior={Platform.OS === "ios" ? "padding" : null}
                                scrollEnabled={false}   
                                keyboardVerticalOffset={100}
          >
            <ScrollView contentContainerStyle={styles.container} 
                        keyboardShouldPersistTaps="handled"
                        scrollEnabled={isScrollable}
            >
              <View style={styles.inner}>
                  <Image
                    style={styles.logo}
                    source={Logo}
                  />
                  <View style={styles.form}>
                    {props.children}

                    <TouchableOpacity onPress={onSubmit}>
                      <View style={styles.auth_mode}>
                        <Text style={styles.auth_text}>{authMode}</Text>
                      </View>
                    </TouchableOpacity>

                    <View style={styles.alternative}>
                        <View style={{ marginVertical : 20 }}>
                          <Text style={{ alignSelf : 'center', fontWeight : 'bold'}}>Où se connecter depuis :</Text>
                        </View>
                        <View style={{ display : 'flex', flexDirection : 'row', alignSelf : 'center'}}>
                          <Image style={styles.social_picto} source={Facebook}></Image>
                          <Image style={styles.social_picto} source={Gmail}></Image>
                        </View>
                    </View>
                  </View>
                  <View style={styles.auth_method}>
                      <Text>{authMethodText}</Text>
                      <TouchableWithoutFeedback onPress={authPage}>
                        <Text style={styles.auth_link}>{authLink}</Text>
                      </TouchableWithoutFeedback>
                    </View>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flexGrow : 1,
    display : 'flex',
    backgroundColor : '#fff',
    position : 'relative',
    paddingBottom : 100
  },
  inner : {
    padding : 20,
  },
  logo :{
    width : 180,
    height : 130,
    alignSelf : 'center',
    marginBottom : 20
  },
  form :{
    borderTopWidth : 1,
    borderBottomWidth : 1,
    borderColor : '#E8E8E8',
    padding: 20,
    marginBottom : 20
  },
  auth_mode : {
    backgroundColor : '#7FCBE2',
    padding : 20,
    borderRadius : 300,
    marginTop : 10
  },
  auth_text: {
    textAlign : 'center',
    fontWeight : 'bold',
    color : '#fff'
  },
  social_picto : {
    width : 50,
    height : 50,
    marginHorizontal : 30
  },
  auth_method :{
    alignSelf : 'center',
    textAlign :'center'
  },
  auth_link : {
    textAlign : 'center', 
    fontWeight : 'bold', 
    textDecorationLine: 'underline',
    marginVertical : 4
  }
})