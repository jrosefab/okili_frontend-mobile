import React from 'react';
import { ScrollView, StyleSheet,
         View, TouchableOpacity } from 'react-native';
import { TabBar } from '../../components';
import Entypo from 'react-native-vector-icons/Entypo'
import FontAwesome from 'react-native-vector-icons/FontAwesome'

console.disableYellowBox = true;

export const RecapLayout = (props) => {
    const { onSubmit, onCancel } = props

    return (
          <View style={styles.container}>
            <ScrollView contentContainerStyle={styles.inner}>
              {props.children}
            </ScrollView>
            <View style={styles.bottomBar}>
              <TouchableOpacity onPress={onSubmit}>
                <TabBar  label="Publier l'annonce" style={{ alignSelf : 'center'}}>
                    <Entypo name="pin" size={30} color="#B2BEC3" onPress={onSubmit} />
                </TabBar>
              </TouchableOpacity>
              <TouchableOpacity onPress={onCancel}>
                <TabBar label="Annuler l'annonce">
                    <FontAwesome name="trash" size={30} color="#FF6A6A"  />
                </TabBar>
              </TouchableOpacity>
            </View>
          </View>
    );
}

const styles = StyleSheet.create({
  stepIndicator : {
    display : 'flex',
    flexGrow : 1,

  },
  container: {
    display : 'flex',
    backgroundColor : '#fff',
    position : 'relative',
    paddingBottom : 70
  },
  inner : {
    padding : 20
  },
  nextStep : {
    position : 'absolute',
    bottom : 30,
    left : '45%',
  },
  stepIllustration : {
    justifyContent : 'center',
    alignSelf:  'center',
    width: 70,
    height : 70,
    marginBottom : 20,
  },
  bottomBar : {
    display : 'flex',
    flexDirection : 'row',
    borderTopWidth : 1,
    borderColor : "#B2BEC3",
    justifyContent: 'space-around',
    alignContent : 'center',
    alignItems : 'center',
    height : 80
  }
});