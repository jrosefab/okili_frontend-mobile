import React from 'react';
import { Text, StyleSheet, 
        View, ScrollView, } from 'react-native';

export const MyAdListLayout = (props) => {
  return (
    <ScrollView horizontal={false} style={styles.layout}>
        <Text style={styles.title}>
            Annonces en cours
        </Text>
        <View style={styles.container}>
          {props.children}
        </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  layout:{
    backgroundColor : '#fff',
    padding : 10,
    position : "relative"
  },
  container: {
    flexGrow: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  title : {
    fontSize : 18, 
    fontWeight : "bold",
    marginLeft : 10 ,
    color : '#6D6D6D'
  }
})