import React, { useState } from 'react';
import { TouchableWithoutFeedback, Keyboard, 
         KeyboardAvoidingView, ScrollView, 
         StyleSheet, Image, Alert, View } from 'react-native';
import { StepCustom, NextStepBtn } from '../../components';

console.disableYellowBox = true;

export const StepLayout = (props) => {
    const { position, illustration, 
            navigation, nextScreen,
            goForward, message } = props
    const [currentPosition, setCurrentPosition] = useState(position);

    const nextStep = () => {
      if (goForward === true){
          navigation.navigate(nextScreen);
          setCurrentPosition(position)
      }else{
        Alert.alert('Champs manquants', message)
      }
    };

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <KeyboardAvoidingView style={{ flex : 1 }}
                              behavior={Platform.OS === "ios" ? "padding" : null}
                              scrollEnabled={false}   
                              keyboardVerticalOffset={100}
        >
          <ScrollView contentContainerStyle={styles.container} 
                      keyboardShouldPersistTaps="handled"
          >
            <View style={styles.inner}>
              <View style={styles.stepIndicator}>
                <Image
                  style={styles.stepIllustration}
                  source={illustration}
                />
                {props.isProposition ?
                  <StepCustom
                    stepCount={3}
                    currentPosition={currentPosition}
                    labels={["Horaires",
                             "Ma proposition",
                             "Prix",
                           ]}
                  />
              :
                <StepCustom
                  stepCount={6}
                  currentPosition={currentPosition}
                  labels={["Catégories", 
                            "Informations",
                            "Etat",
                            "Urgence",
                            "Livraison",
                            "Prix",
                          ]}
                />
              }
              </View>
              {props.children}
            </View>
            <NextStepBtn style={styles.nextStep} onPress={() => nextStep() } />
          </ScrollView>
        </KeyboardAvoidingView>
      </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
  stepIndicator : {
    display : 'flex',
  },
  container: {
    flexGrow : 1,
    display : 'flex',
    backgroundColor : '#fff',
    position : 'relative',
    paddingBottom : 100
  },
  inner : {
    padding : 20
  },
  nextStep : {
    position : 'absolute',
    bottom : 30,
    left : '45%',
  },
  stepIllustration : {
    justifyContent : 'center',
    alignSelf:  'center',
    width: 70,
    height : 70,
    marginBottom : 20,
  }
});