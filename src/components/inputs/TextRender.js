import React from 'react';
import { View, TextInput, 
        StyleSheet } from 'react-native';

export const TextRender = ( props ) => {
    const { input, style, keyboardType, ...inputProps } = props;
    return (
        <View style={[styles.inputContainer, style]}>
            <TextInput
                onChangeText={input.onChange}
                onBlur={input.onBlur}
                onFocus={input.onFocus}
                value={input.value.toString()}
                keyboardType={keyboardType}
                multiline={true}
                {...inputProps}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    inputContainer : {
        borderWidth : 1,
        borderColor : '#B2BEC3',
        borderRadius : 300,
        padding : 5,
        marginHorizontal : 3
    }
});