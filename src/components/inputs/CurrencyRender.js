import React, { useState } from 'react';
import { View, Text, 
        StyleSheet } from 'react-native';
import NumericInput from '@wwdrew/react-native-numeric-textinput';

export const CurrencyRender = ( props ) => {
    const { value, input, ...inputProps } = props;
    return (
        <View style={{ position : 'relative' }}>
            <NumericInput
                placeholder={'0.00'}
                style={styles.price}
                decimalPlaces={2}
                type='decimal'
                value={input.value}
                onUpdate={input.onChange}
                {...inputProps}
            />
            <Text style={[styles.symbol, !props.meta.pristine ? { opacity : 1  } 
                                                             : { opacity : 0.4}]}>
                €
            </Text>
        </View>

    );
}

const styles = StyleSheet.create({
    price : {
        textAlign : 'center',
        fontSize : 40,
        borderRadius : 100,
        borderWidth : 2,
        borderColor : '#888888',
        marginHorizontal : 30,
    },
    symbol : {
        position : 'absolute',
        marginHorizontal : 60,
        right : 0,
        top : 10,
        fontSize : 40,
    }
});