import React from 'react';
import { TouchableWithoutFeedback, Text, 
         View, StyleSheet } from 'react-native';

export const RadioRender = (props) => {
    const { radios, input:{ value, onChange } } = props;
    return (
        <View>
            { radios.map( radio => 
                <Radio key={radio.label} {...radio} 
                       onChange={onChange} 
                       checked={radio.value === value} /> 
            )}
        </View>
    )   
}

const Radio = (props) => {
    const { checked, label } = props;    
    const handlePress = () => props.onChange(props.value);
    
    return (
        <TouchableWithoutFeedback onPress={handlePress}>
            <View style={styles.radioWrapper}>
                <View style={styles.radio}>
                    <View style={[styles.checked, checked ? styles.circleFilled : styles.circle ]} />
                </View>
                <View>
                    <Text style={{ fontSize : 16, color : '#848484'  }}>{label}</Text>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    radioWrapper : {
        display : 'flex',
        flexDirection : 'row',
        alignItems : 'center'
    },
    radio : {
        display : 'flex',
        alignContent : 'center',
        alignItems : 'center',
        justifyContent : 'center',
        borderColor : '#4B4B4B',
        marginRight : 20,
        marginVertical : 10,
        borderWidth : 1,
        width : 20,
        height : 20,
        borderRadius : 100,
        position : 'relative'
    },
    checked : { 
        borderColor : '#4B4B4B',
        width : 13,
        height : 13,
        top : 2.5,
        borderRadius : 100,
        position : 'absolute'
    },  
    circleFilled: {
      backgroundColor: '#4B4B4B',
    },
    circle: {
        backgroundColor: 'transparent',
    },
});


