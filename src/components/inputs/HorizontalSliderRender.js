import React from "react";
import Slider from "react-native-slider";
import { View, StyleSheet, Text } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';

export const HorizontalSliderRender = ( props ) => {
  const { custom, input, ...inputProps } = props
  return (
    <View style={styles.container}>
        <View style={styles.infokm}>
            <Entypo name="location-pin" size={25} />
            <Text style={styles.text}>{input.value} km</Text>
        </View>
        <Slider
            value={input.value}
            onValueChange={input.onChange}
            trackStyle={{ height : 10, borderRadius : 5 }}
            minimumValue={0}
            maximumValue={10}
            minimumTrackTintColor={'#FFB25A'}
            maximumTrackTintColor={'#DBDBDB'}
            thumbStyle={styles.thumb}
            step={1}
            {...inputProps}
        />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    alignItems: "stretch",
    justifyContent: "center"
  },
  thumb : {
    width: 30,
    height: 30,
    borderRadius : 100,
    borderColor : '#232323',
    borderWidth : 4,
    backgroundColor : '#fff'
  },
  infokm : {
    display : 'flex',
    alignItems: 'center',
    flexDirection : 'row',
    justifyContent :'center'
  },
  text : {
    fontSize : 22
  },
})