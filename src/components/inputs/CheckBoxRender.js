import React, { useState } from 'react';
import CheckBox from '@react-native-community/checkbox';
import { StyleSheet } from 'react-native';

export const CheckBoxRender = (props) => {
    const { input, ...inputProps} = props
    const [toggleCheckBox, setToggleCheckBox] = useState(false)
    return (
        <CheckBox style={styles.CheckBox}
                  disabled={false}
                  value={toggleCheckBox}
                  onValueChange={ () => toggleCheckBox ? setToggleCheckBox(false) : setToggleCheckBox(true)}
                  {...inputProps}
        />
    )
}

const styles = StyleSheet.create({
    CheckBox: {
        borderColor : 'blue',
        display : 'flex',
        alignSelf :'flex-start',
    },
});