import React, { useState, useEffect } from "react";
import { View, Text, Image,
        StyleSheet, TouchableOpacity } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Entypo from 'react-native-vector-icons/Entypo';

const options = {
    title: 'Choisir une image',
    takePhotoButtonTitle : 'Prendre une photo',
    chooseFromLibraryButtonTitle :'Choisir dans la gallerie',
    mediaType : 'photo',
    cameraType: 'back',
    noData : true,
    allowsEditing : true,
    quality : 0.7,
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
};

export const ImagePickerRender = ( props ) => {
    const { deleteButton, defaultImage, 
            style, image, input:{ onChange }, 
            clearField, ...inputProps } = props
    return (
        <View>
            <UploadImage onChange={onChange} 
                         style={[styles.cameraContainer, style]}
                         image={image}
                         {...inputProps}
                         defaultImage={defaultImage}
                         deleteButton={deleteButton}
                         clearField={clearField}
            />
        </View>
    );
}

const UploadImage = (props) => {
    var { style, defaultImage } = props;
    const [picture, setPicture] = useState(defaultImage)
    const pictureChoice = () => {
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
                return response.didCancel;
            } else if (response.error) {
              return response.error;
            } else {
                setPicture({ 
                    uri      : response.uri,
                    type     : response.type,
                    fileName :  response.fileName 
                });
            }
        });
    }

    useEffect(() => {
        if (picture !== defaultImage){
            const sendPicture = async() => {
                await props.onChange(picture)
            }; 
            sendPicture()
            props.onChange(picture)
        }else{
            setPicture(defaultImage)
        }
    }, [picture])
    
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={pictureChoice}>
                <View style={[styles.camera, { position : 'relative'}]}>
                    <Image style={style} 
                           source={picture === defaultImage ? picture 
                                  : { uri : picture.uri }}
                    />
                </View>
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        display : 'flex',
        alignSelf :'flex-start',
    },
    camera : {
        alignSelf : 'center',
    },
    text : { 
        fontSize : 10,
        textAlign : 'center',
        padding : 5
    },
    add : {
        color : '#87D15C',
        position : 'absolute',
        bottom : 0,
        left : 105,
    },
    delete : {
        color : '#FF7D86',
        position : 'absolute',
        bottom : 0,
        left : 105,
        zIndex: 10,
    },
});