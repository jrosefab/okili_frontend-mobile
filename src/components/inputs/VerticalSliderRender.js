import React from "react";
import Slider from 'rn-vertical-slider';

export const VerticalSliderRender = ( props ) => {
    const { custom, input, ...inputProps } = props
    return (
        <Slider
          value={input.value}
          disabled={false}
          min={0}
          max={100}
          onChange={input.onChange}
          width={100}
          height={130}
          step={1}
          minimumTrackTintColor={custom.minTrack}
          maximumTrackTintColor={custom.maxTrack}
          showBallIndicator={true}
          ballIndicatorWidth={40}
          animationDuration={0}
          ballIndicatorColor={"#87D15C"}
          ballIndicatorTextColor={"#fff"}
          showBackgroundShadow={false}
          {...inputProps}
        />
    );
};