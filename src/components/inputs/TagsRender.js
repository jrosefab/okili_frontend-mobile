import React from "react";
import { TouchableOpacity, 
        StyleSheet, Text } from "react-native";
import Tags from "react-native-tags";

export const TagsRender = (props) => {
    const {initialTags, input, ...inputProps} = props;
    var tagsArray = [];
    return (
    <Tags style={styles.container}
        maxNumberOfTags={4}
        initialTags={initialTags}
        textInputProps={{ onBlur : () => input.onBlur(tagsArray) }}
        onChangeTags={(tag) => { tagsArray = tag; }}
        containerStyle={{ justifyContent: "center" }}
        inputStyle={{ backgroundColor: "white" }}
        renderTag={({ tag, index, onPress }) => (
                    <TouchableOpacity style={styles.tag} key={`${tag}-${index}`} onPress={onPress}>
                        <Text style={{ color : '#fff', marginRight : 10 }}>x</Text>
                        <Text style={{ color : '#fff' }}>{tag}</Text>
                    </TouchableOpacity>
                )}
        {...inputProps}
    />)
}

const styles = StyleSheet.create({
    container : { 
        borderWidth : 1,
        borderColor : '#B2BEC3',
        borderRadius : 300,
        padding : 8
    },
    tag : {
        backgroundColor : '#2CBDF3',
        borderRadius : 15,
        display : 'flex',
        flexDirection : 'row',
        alignContent : 'center',
        justifyContent : 'space-between',
        paddingVertical : 5,
        paddingHorizontal : 20,
        margin : 2
    },
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    }
})