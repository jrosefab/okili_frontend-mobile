import React from 'react';
import { View, Text, StyleSheet,
    TouchableWithoutFeedback } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';


export const BoxSelectRender = (props) => {
    const { box, input:{ onChange } } = props;
    return (
        <View>
                <BoxSelect onChange={onChange}
                           icon={box.icon}
                           backgroundColor={box.background}
                           text={box.text}
                           opacity={box.opacity}
                           {...box }
                />

        </View>

    )   
}

const BoxSelect = ( props ) => { /* Vérifier pk isChecked == false*/
    const { icon, backgroundColor, text, opacity } = props
    const handlePress = () => props.onChange(props.value);
    return (
        <TouchableWithoutFeedback onPress={handlePress}>
            <View style={[styles.delivery, { opacity : opacity }]} value={props.value}>
                <View style={[styles.picto, { backgroundColor : backgroundColor }]
                            }>
                    <FontAwesome5 name={icon} color={'#fff'} size={45}/>
                </View>
                    <Text style={styles.text}>{text}</Text>
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    delivery_wrapper : {
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'space-around',
        
    },
    unchecked : {
        opacity : 0.8
    },  
    delivery : {
        display : 'flex',
        justifyContent: 'center',
    },
    picto : {
        borderRadius : 15,
        padding : 30,
        marginVertical : 2
    },
    text : {
        alignSelf : 'center', 
        fontSize : 20,
        color : '#717171'
    }
})