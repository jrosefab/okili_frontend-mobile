import React, { useState } from 'react';
import { View, TextInput, 
        Text, StyleSheet } from 'react-native';

export const EmailRender = ( props ) => {
    const { input, ...inputProps } = props;
    const [value, setValue] = useState({ email : "" })
    const [error, setError] = useState({ error : "" })

    const validate = () => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(value.email) === false) {
            setError({ error : "Votre adresse est incorrect"})
        }else{
            setError({ error : "" })
            console.log(error.error)
            input.onChange(value.email)
        }
      ;
    }

    return (
        <>
            {error && <Text style={styles.error_text}>{error.error}</Text>}
            <View style={[styles.inputContainer, error.error && styles.error_field ]}>
                <TextInput
                    onChangeText={text => setValue({email : text})}
                    onFocus={input.onFocus}
                    onBlur={() => validate()}
                    {...inputProps}
                />
                
            </View>
        </>
    );
}

const styles = StyleSheet.create({
    inputContainer : {
        borderWidth : 1,
        borderColor : '#B2BEC3',
        borderRadius : 300,
        padding : 5,
        marginHorizontal : 3
    },
    error_field : {
        borderWidth : 1,
        borderColor : '#FF7D86',
        borderRadius : 300,
    },
    error_text : {
        position : "absolute",
        color : '#FF7D86',
        alignSelf : 'center'
    }
});

