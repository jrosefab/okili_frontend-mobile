import React, { useState } from 'react';
import { REACT_APP_MAPS_API_KEY } from 'react-native-dotenv';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { StyleSheet, View, Keyboard,
         KeyboardAvoidingView, 
         TouchableWithoutFeedback } from 'react-native';

         export const GooglePlacesAutoRender = ( props ) => {
    const {input, ...inputProps } = props;

    // If user set new location, update states
    const setNewLocation = (data) => {
        var address;
        address = data.description
        input.onChange(address);
    };

    return (
        
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <KeyboardAvoidingView style={{ flex : 1 }}
                                      behavior={Platform.OS === "ios" ? "padding" : null}
                                      scrollEnabled={false}   
                                      keyboardVerticalOffset={100}
                >
                    <GooglePlacesAutocomplete
                        styles={{
                            textInputContainer: {
                                borderRadius : 300,
                                borderColor : '#B2BEC3',
                                backgroundColor : '#fff',
                                borderWidth : 1,
                                borderTopWidth : 1, 
                                borderTopColor :'#B2BEC3',
                                borderBottomWidth : 1, 
                                borderBottomColor :'#B2BEC3',
                                height : 60,
                            },
                            textInput: {
                                fontSize: 14,
                                height : 45,
                            },
                            listView : {
                                padding : 20,
                            },
                            predefinedPlacesDescription: {
                                color: '#1faadb',
                            },
                        }}
                        currentLocation={true}
                        keyboardShouldPersistTaps='always'
                        listViewDisplayed='false'
                        enablePoweredByContainer={false}
                        minLength={2} // minimum length of text to search
                        returnKeyType={'search'} // Can be left out for default return key 
                        fetchDetails={true}
                        onPress={(data) => { // 'details' is provided when fetchDetails = true
                            setNewLocation(data);
                        }}
                        query={{
                            key:`${REACT_APP_MAPS_API_KEY}`,
                            language: 'fr',
                            components: 'country:fr'
                        }}
                        nearbyPlacesAPI='GooglePlacesSearch'
                        debounce={300}
                        onFail={error => console.error(error)}
                        disableScroll
                        {...inputProps}
                    />
                </KeyboardAvoidingView>
            </TouchableWithoutFeedback>
)};

const styles = StyleSheet.create({
    custom : {
        paddingBottom : 0,
        padding: 30 ,
    }
});