import React, { useState, useRef, useEffect } from 'react';
import { REACT_APP_MAPS_API_KEY } from 'react-native-dotenv';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import MapView, { Marker } from 'react-native-maps';
import { View, Text, ScrollView, StyleSheet, 
         KeyboardAvoidingView, Keyboard,
         TouchableWithoutFeedback, } from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export const GeoLocationRender = (props) => {
    const {IslocationAvailable, actualLocation, 
            placeholder, input, ...inputProps } = props;
    return (
        <View>
            <SetGeoLocation onChange={input.onChange}
                            {...inputProps}
                            actualLocation={actualLocation}
                            IslocationAvailable={IslocationAvailable}
            />
        </View>
    );
};

/* combine input and map */
const SetGeoLocation = ( props ) => {
    const { actualLocation, IslocationAvailable } = props;
    const [ currentLocation, setCurrentLocation ] = useState({ address   : "",
                                                               longitude : 0, 
                                                               latitude  : 0
                                                            })                                      

    //when mounted, set actual location in props
    const submitUserLocation = () => {
        if (!currentLocation.address){
            props.onChange(actualLocation);
        }else{
            props.onChange(currentLocation);
        };
    };

    // If user set new location, update states
    const setNewLocation = (data, details) => {
        setCurrentLocation({ 
            address   : data.description,
            longitude : details.lng, 
            latitude  : details.lat
        });
    };
    
    // inject value by onChange method in schema form.values.geolocation
    useEffect(() => {
        setCurrentLocation(actualLocation);
    }, []);

    return (
        <View>
            <View style={{ marginBottom : 10}}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <KeyboardAvoidingView style={{ flex : 1 }}
                                          behavior={Platform.OS === "ios" ? "padding" : null}
                                          scrollEnabled={false}   
                                          keyboardVerticalOffset={100}
                    >
                        <GooglePlacesAutocomplete
                            styles={{
                                textInputContainer: {
                                    backgroundColor: 'rgba(0,0,0,0)',
                                    borderTopWidth: 0,
                                    borderBottomWidth: 0,
                                },
                                textInput: {
                                    height: 60,
                                    width : '100%',
                                    fontSize: 20,
                                    marginHorizontal : 0,
                                    borderRadius : 300,
                                    borderColor : '#B2BEC3',
                                    borderWidth : 1,
                                    padding : 0
                                },
                                listView : {
                                    padding : 20,
                                },
                                predefinedPlacesDescription: {
                                    color: '#1faadb',
                                },
                            }}
                            currentLocation={true}
                            placeholder={actualLocation.address}
                            keyboardShouldPersistTaps='always'
                            listViewDisplayed='false'
                            enablePoweredByContainer={false}
                            minLength={2} // minimum length of text to search
                            fetchDetails={true}
                            onPress={(data, details) => { // 'details' is provided when fetchDetails = true
                                setNewLocation(data, details.geometry.location);
                            }}
                            query={{
                                key:`${REACT_APP_MAPS_API_KEY}`,
                                language: 'fr',
                                components: 'country:fr'
                            }}
                            nearbyPlacesAPI='GooglePlacesSearch'
                            debounce={300}
                            onFail={error => console.error(error)}
                            disableScroll
                        />
                    </KeyboardAvoidingView>
                </TouchableWithoutFeedback>
            </View>
            <ScrollView>
                <View style={styles.mapContainer}>
                    {!IslocationAvailable &&
                        <View style={styles.blockMap}>
                            <View style={styles.locationOff}>
                                <MaterialIcons style={{alignSelf : 'center'}} name="location-off" size={40} color={"#fff"} />
                                <Text style={{ color : '#fff' }}>Localisation indisponible.</Text>
                            </View>
                        </View>
                    }
                    <MapView style={styles.map}
                            followsUserLocation={true}
                            onMapReady={submitUserLocation()}
                            region={{ 
                                latitude: currentLocation.latitude,
                                longitude: currentLocation.longitude, 
                                latitudeDelta: 0.005,
                                longitudeDelta: 0.005,
                            }}
                    >
                        <Marker.Animated style={{ position: 'absolute', top: 100, left: 50 }}
                                        coordinate={{ 
                                            latitude: currentLocation.latitude,
                                            longitude: currentLocation.longitude 
                                        }}
                                        title={"Lieu de retrait"}
                                        description={"Domicile"}
                                        draggable
                        />
                    </MapView>
                </View>
            </ScrollView>
        </View>
)};

const styles = StyleSheet.create({
    mapContainer : {
        height: 250,
        width: 350,
        alignSelf : "center",
        marginTop :30,
        position :'relative'
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    blockMap : {
        position : 'absolute',
        height : 250,
        width: 350,
        zIndex : 99,
        backgroundColor : 'rgba(0,0,0,0.6)'
    },
    locationOff : {
        position :'absolute',
        alignSelf : 'center',
        alignContent : 'center',
        top : '32%',
        color : '#fff'
    }
});