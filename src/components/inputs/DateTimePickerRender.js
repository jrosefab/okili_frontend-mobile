import React, { useState } from 'react';
import {View, Button, Platform, 
        Text, StyleSheet} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import 'moment/locale/fr';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

export const DateTimePickerRender = ( props ) => {
    const { input, ...inputProps } = props;
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    
    const onChange = (event, selectedDate) => {
      const currentDate = selectedDate || date;
      setShow(Platform.OS === 'ios');
      setDate(currentDate);
      input.onChange(currentDate)
    };
  
    const showMode = currentMode => {
      setShow(true);
      setMode(currentMode);
    };
  

    return (
        <View>
            <View style={{ marginBottom : 20 }}>
                <Text style={styles.title}>Quel jour ?</Text>
                <TouchableWithoutFeedback onPress={() => showMode('date')}>
                    <View style={styles.inputContainer}>
                        <Text style={styles.text}>{date.toLocaleDateString()}</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>
            <View style={{ marginBottom : 20 }}>
                <Text style={styles.title}>À quelle heure ?</Text>
                <TouchableWithoutFeedback onPress={() => showMode('time')}>
                    <View style={styles.inputContainer}>
                        <Text style={styles.text}>{date.toLocaleTimeString()}</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View>

            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                    {...inputProps}
                />
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    inputContainer : {
        borderWidth : 1,
        borderColor : '#B2BEC3',
        borderRadius : 300,
        padding : 20,
        marginHorizontal : 3
    },
    title : {
        marginVertical : 10,
        fontSize : 18,
        paddingLeft : 10,
    },
    text:{
        alignSelf : 'center',
        fontSize : 18
    }
});