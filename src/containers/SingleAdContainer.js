import React, { useEffect,useRef, useLayoutEffect } from 'react';
import moment from 'moment';
import 'moment/locale/fr';
import { bindActionCreators } from 'redux';
import { Text, View, StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux';
import { getUserAction } from '../store/actions/user.actions';
import { AdListedCard,  SingleAdLayout, Loading } from '../components'
import { REACT_APP_OKILI_BACK } from 'react-native-dotenv';
import Entypo from 'react-native-vector-icons/Entypo'
import Water from '../assets/illustrations/water.png';
import Bottle from '../assets/illustrations/bottle.png'

function SingleAd(props){
    const { authenticate, userData, ads, navigation } = props

    const getCreatedDate = (data) => (
        moment(data).locale('fr').startOf('hour').fromNow()
    );

    const usePrevious = (value) => {
        const ref = useRef();
        useEffect(() => {
          ref.current = value;
        });
        return ref.current;
    }
    const prevAd = usePrevious(ads.singleAd)

    useLayoutEffect(() =>{
        if (prevAd !== ads.singleAd){
            props.getUser(ads.singleAd.created_by.profileId);
        }
    }, [ads.singleAd] );

    return (
        <>
            {ads.singleAd && 
                userData && userData.user &&
                    <SingleAdLayout 
                        onProposal={() => navigation.navigate('proposal_meeting')}
                        dateTime={getCreatedDate(ads.singleAd.created_date)}
                        longitude={ads.singleAd.geolocation.longitude}
                        latitude={ads.singleAd.geolocation.latitude}
                        isMine={ads.singleAd.created_by.profileId === authenticate.profileId ? true : false }
                        navigation={navigation}
                        picture={`${REACT_APP_OKILI_BACK}/${ads.singleAd.pictures[0].source}`}
                        isShopping={ads.singleAd.kind === "shopping" ? true : false }
                        onShare={() => { console.log('sharing')}}
                        onSignal={() => { console.log('signal')}}
                        adTtile={ads.singleAd.title}
                        stars={userData.user.stars}
                        description={ads.singleAd.details_content}
                        price={`${ads.singleAd.price} €`}
                        creator={`${userData.user.lastname} ${userData.user.firstname}`}
                        isVerified={userData.user.phone_number.isVerified ? true : false }
                        creatorPicture={userData.user.picture.source && `${REACT_APP_OKILI_BACK}/${userData.user.picture.source}`}
                        haveProposals={true}
                        condition={ ads.singleAd.current_condition.condition !== "null" ? 
                                    <Text style={styles.content}>{ads.singleAd.current_condition.condition}</Text> : 
                                    <>
                                        <View style={styles.condition}>
                                            <View style={styles.filledBottle}>
                                                <Image
                                                    source={Water}
                                                    style={[styles.water, { height : `${ads.singleAd.current_condition.quantity_left}%` }]}
                                                />
                                                <Image
                                                    style={styles.bottle}
                                                    source={Bottle}
                                                />
                                            </View>
                                        </View>
                                        <Text style={[styles.content, { alignSelf : 'center' }]}>{ads.singleAd.current_condition.quantity_left}% restant</Text>
                                    </>
                        }
                        delivery={                    
                            <View>
                                { ads.singleAd.kind === "research" ?
                                    <>
                                        {ads.singleAd.delivery_mode.home_pickup && 
                                            <>
                                                <Text style={styles.content}>
                                                    {`Article a livrer a l'adresse :\n${ads.singleAd.geolocation.address}\n`}
                                                </Text>
                                                <Text  style={styles.content}>
                                                    {ads.singleAd.delivery_mode.home_pickup_details === "null" ? "Pas de détails d'accès à l'adresse.\n" 
                                                    : `Détails d'accès à l'adresse : \n${ads.singleAd.delivery_mode.home_pickup_details}\n`}
                                                </Text>
                                            </>
                                        }
                                        {ads.singleAd.delivery_mode.home_delivery && 
                                            <Text style={styles.content}>
                                                <Entypo name="location-pin" size={16}/>
                                                {userData.user.firstname} peut se déplacer jusqu'à {ads.singleAd.delivery_mode.delivery_km_acceptation}km 
                                            </Text> 
                                        }
                                    </>
                                :
                                    <>
                                        { ads.singleAd.delivery_mode.home_pickup && 
                                            <>
                                                <Text style={styles.content}>
                                                    {`Article a retier a l'adresse :\n${ads.singleAd.geolocation.address}\n`}
                                                </Text>
                                                <Text style={styles.content}>
                                                    {ads.singleAd.delivery_mode.home_pickup_details === "null" ? "Pas de détails d'accès à l'adresse.\n" 
                                                    : `Détails d'accès à l'adresse : \n${ads.singleAd.delivery_mode.home_pickup_details}\n`}
                                                </Text>
                                            </>
                                        }
                                        { ads.singleAd.delivery_mode.home_delivery && 
                                            <Text style={styles.content}>
                                                <Entypo name="location-pin" size={16}/>
                                                {userData.user.firstname} peut livrer jusqu'à {ads.singleAd.delivery_mode.delivery_km_acceptation}km 
                                            </Text> 
                                        }
                                    </>
                                }   
                            </View>
                        }
                    >
                    </SingleAdLayout>
            }
        </>
    )
}

const styles = StyleSheet.create({
    is_logged : {
       height : 200,
    },
    filledBottle : {
        position : 'relative',
    },
    bottle : {
        width: 80,
        height : 100,
        position : 'relative'
    },   
    water : {
        position : 'absolute',
        width: '100%',
        bottom : 0
    },
    condition : {
        display : 'flex', 
        justifyContent : 'space-around',
        flexDirection : 'row', 
        alignItems : 'center'
    },
    content : {
        fontSize : 16,
        opacity : 0.6,
    },
});
const mapStateToProps = (state) => {
    return {
        ads : state.adsReducer,
        authenticate : state.authReducer,
        userData : state.userReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const dispatchesProps = {
          getUser : getUserAction
    };
    return bindActionCreators(dispatchesProps, dispatch);
}

export const SingleAdContainer = connect(mapStateToProps, mapDispatchToProps)(SingleAd)
