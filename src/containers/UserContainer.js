import React, { useEffect }from 'react';
import { getUserAction } from '../store/actions/user.actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { UserLayout } from '../components';
import { View, Text } from 'react-native';

function User(props){
    return (
        <UserLayout>
            <Text>UserLayout</Text>
        </UserLayout>
    )
}
const mapStateToProps = (state) => {
    return {
        user : state.userReducer,
    }
}

export const UserContainer = connect(mapStateToProps)(User);