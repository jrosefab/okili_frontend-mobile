import React, { useEffect } from 'react';
import { Field, change, reduxForm, isDirty } from 'redux-form';
import { View,Image, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import {StepLayout, TextRender, RadioRender,
        VerticalSliderRender, ImagePickerRender} from '../../components';
import Condition from '../../assets/illustrations/condition.png';
import Bottle from '../../assets/illustrations/bottle.png';
import Water from '../../assets/illustrations/water.png';
import Camera from '../../assets/illustrations/camera.png';

function ProposalOffer( props ){
    const  { ads, navigation, conditionFilled, 
             proposal, quantityFilled, picture } = props;

    return (
        <StepLayout navigation={navigation} 
                    isProposition={true}
                    position={1} 
                    goForward={/*conditionFilled || quantityFilled ? */true /*: false */}
                    message={"un ou plusieurs champs(*) contiennent une erreur"}
                    nextScreen={'proposal_price'}
                    illustration={Condition}
        >
            {ads.singleAd.category === "consumable" ?
                <>
                    <Text style={styles.highTitle}>Saisir la quantité restante de votre bien ?*</Text>
                    <Text style={{ alignSelf : 'center', fontSize : 10, marginBottom : 20}}>
                        Appuyer sur la bouteille
                    </Text>
                    <View style={styles.inputContainer}>
                        <View style={styles.slider}>
                            <Field name="quantity_left" 
                                    custom={{ minTrack : 'transparent', 
                                                maxTrack : 'transparent'}} 
                                    component={VerticalSliderRender}
                            /> 
                        </View>
                        {proposal && proposal.values &&
                            <View style={styles.filledBottle}>
                                <Image
                                    style={styles.water}
                                    source={Water}
                                    style={[styles.water, { height : proposal.values.quantity_left === null ? 
                                                            '0%' : `${proposal.values.quantity_left}%` }
                                        ]}
                                />
                                <Image
                                    style={styles.bottle}
                                    source={Bottle}
                                />
                            </View>
                        }
                    </View>

                </>
            :
                <View>
                    <Text style={styles.highTitle}>Quel est l'état de votre bien ?*</Text>
                    <Field name="condition"
                        radios={[{ label : 'Neuf', value:'new'},
                                  { label : 'Très bon état', value:'good'},
                                  { label : 'État moyen, neutre', value:'neutral'},
                                  { label : 'Mauvais état', value:'bad'},
                                  { label : 'Pour pièces', value:'hs'}, ]}
                        component={RadioRender}
                    />
                </View>
            }

            <Text style={styles.highTitle}>Ajouter une photo*</Text>
            <Field style={styles.camera} 
                    defaultImage={Camera} 
                    name="picture_proposal" 
                    source={ picture && { uri : picture.uri } } 
                    component={ImagePickerRender}
            />
            
            <Text style={styles.highTitle}>Souhaitez-vous ajouter quelque-choses ?</Text>
            <Field name="details_content" 
                placeholder="Interphone, numéro de téléphone ou précision d'emplacement..."
                component={TextRender}
            />
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    },
    inputContainer:{
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'center',
        alignContent :'center',
        alignItems :'center'
    },  
    slider : { 
        position: "absolute",
        zIndex : 5,
    },
    filledBottle : {
        position : 'relative'
    },
    bottle : {
        width: 120,
        height : 140,
        position : 'relative'
    },   
    water : {
        position : 'absolute',
        width: '100%',
        bottom : 0
    },
    camera : {
        borderRadius : 20,
        height : 80,
        width : 80,
        backgroundColor : '#E4E4E4'
    },
    alert : {
        textAlign : 'justify',
        marginVertical : 20,
        marginHorizontal : 20
    }
})

const mapStateToProps = state => {
    return { 
        ads : state.adsReducer,
        proposal : state.form.proposalForm,
        quantityFilled:isDirty('proposalForm')(state, ["quantity_left"]), 
        pictureFilled:isDirty('proposalForm')(state, ["picture_proposal"])
    };
};

ProposalOffer = reduxForm({ form : "proposalForm",
                                initialValues : { 
                                    category: null,
                                    kind : "research",
                                    home_delivery : false,
                                    home_pickup : true,
                                    home_pickup_details : null,
                                    delivery_km_acceptation : 0,
                                    condition : null,
                                    quantity_left : null
                                },
                            destroyOnUnmount: false,
                        })(ProposalOffer);

export const ProposalOfferContainer = connect(mapStateToProps, null)(ProposalOffer);