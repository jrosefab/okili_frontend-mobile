import React, { useState } from 'react';
import { Field, change, reduxForm, isDirty } from 'redux-form';
import { View,TouchableOpacity, Text, Modal, 
         StyleSheet, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import {StepLayout, CurrencyRender } from '../../components';
import Condition from '../../assets/illustrations/condition.png';
import Bottle from '../../assets/illustrations/bottle.png';
import Water from '../../assets/illustrations/water.png';
import Camera from '../../assets/illustrations/camera.png';

function ProposalPrice( props ){
    const  { ads, navigation, priceFilled, 
             acceptedPaymentFilled, proposal, picture } = props;
    const [modalVisible, setModalVisible] = useState(false)

    return (
        <StepLayout navigation={navigation} 
                    isProposition={true}
                    position={2} 
                    goForward={true}
                    message={"un ou plusieurs champs(*) contiennent une erreur"}
                    nextScreen={'proposal_recap'}
                    illustration={Condition}
        >{console.log(acceptedPaymentFilled)}
            <Text style={styles.highTitle}>
                {ads.singleAd.kind === "research" ?  
                    "Vous serez payé" 
                : 
                    "Faire une offre"
                }
            </Text>
            <View>
            {ads.singleAd.kind === "research" ?  
                <View>
                    <Text style={styles.price}>
                    {!acceptedPaymentFilled ? 
                        `${ads.singleAd.kind} €`
                        : 
                        `${proposal.values.accepted_payment} €`
                    }
                    </Text>
                    <TouchableOpacity onPress={() => setModalVisible(true)}>
                        <Text style={styles.text_proposal}>
                            Proposer un autre prix
                        </Text>
                    </TouchableOpacity>

                </View>

                :
                    <>
                        <Field name="price" value={1} component={CurrencyRender}/> 
                        <Text style={styles.alert}>
                            {ads.singleAd.kind}
                        </Text>
                    </>
            }
            </View>
            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(!modalVisible)}
            >
                <TouchableWithoutFeedback onPress={() => setModalVisible(!modalVisible)}>
                    <View style={styles.centeredView}>
                        <View style={styles.modal_container}>
                            <Field name="accepted_payment" autoFocus value={1} component={CurrencyRender}/> 
                            <Text style={styles.alert}>
                                Attention, le prix conseillé vous assure 
                                de conclure marché dans les meilleurs délais.
                            </Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    },
    alert : {
        textAlign : 'justify',
        marginVertical : 20,
        marginHorizontal : 20
    },
    price : {
        textAlign : 'center',
        fontSize : 40,
        borderRadius : 100,
        borderWidth : 2,
        borderColor : '#888888',
        marginHorizontal : 30,
        padding : 10
    },
    text_proposal : {
        color : '#7FCBE2',
        textAlign : 'center',
        marginVertical : 5
    },
    centeredView: {
        flex : 1,
        justifyContent :'center',
        marginBottom : 0,
        backgroundColor : "rgba(0, 0, 0, 0.72)"
      },
    modal_container : {
        backgroundColor : "#fff",
        height : 200,
        width : '90%',
        alignSelf : 'center',
        margin : 5,
        padding : 20,
        borderRadius : 30,
    },
})

const mapStateToProps = state => {
    return { 
        ads : state.adsReducer,
        proposal : state.form.proposalForm,
        priceFilled:isDirty('proposalForm')(state, ["price"]), 
        acceptedPaymentFilled:isDirty('proposalForm')(state, ["accepted_payment"]), 

    };
};

ProposalPrice = reduxForm({ form : "proposalForm",
                                initialValues : { 
                                    category: null,
                                    kind : "research",
                                    home_delivery : false,
                                    home_pickup : true,
                                    home_pickup_details : null,
                                    delivery_km_acceptation : 0,
                                    condition : null,
                                    quantity_left : null
                                },
                            destroyOnUnmount: false,
                        })(ProposalPrice);

export const ProposalPriceContainer = connect(mapStateToProps, null)(ProposalPrice);