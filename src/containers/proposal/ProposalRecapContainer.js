import React, {useState} from 'react';
import { reduxForm, reset } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Text, View, StyleSheet, 
         Image,  Alert} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import { adsActions } from '../../store/actions/ads.actions';
import { RecapLayout, Map, Loading, ModalLogin } from '../../components';
import Bottle from '../../assets/illustrations/bottle.png';
import Water from '../../assets/illustrations/water.png';
import HourGlass from '../../assets/illustrations/hourglass.png';

function ProposalRecap(props){
    const { user, proposal, ads, navigation, authenticate } = props;
    const [modalVisible, setModalVisible] = useState(false)

    const submitAd = (ad) => {
       if(!authenticate.profileId ){
           setModalVisible(true) 
       }else{
           props.createAd(ad)
           navigation.navigate('home'); 
           props.dispatch(reset('createSaleForm'))
       }
    }
    const deleteFormAd = () => {
        Alert.alert(
            'Attention',
            'Vous êtes sur le point de supprimer votre annonce', // <- this part is optional, you can pass an empty string
            [
                {text: 'Annuler', onPress: () => { setModalVisible(false)}, style: 'cancel' },
                {text: 'Supprimer', onPress: () => {
                    navigation.navigate('home'); 
                    props.dispatch(reset('createSaleForm'));
                }},
            ],
            {cancelable: true},
        );
    };
    
    return (
        <RecapLayout 
            onSubmit={() => submitAd(proposal.values)}
            onCancel={() => deleteFormAd()}
        >
            <Text style={styles.highTitle}>Votre offre est en attente d'achat</Text>
            <Image source={HourGlass} style={styles.hourglass}/>
            <Text style={[styles.highTitle, { marginBottom : 0 }]}>Récapitulatif :</Text>
            <View style={styles.recapContainer}>
                <View style={{ position : 'relative' }}>
                    <Text style={styles.miniTitle}>Vous vendez :</Text>
                    <View style={styles.inSale}>
                        <View style={{ paddingHorizontal : 20, flexShrink : 1 }}>

                        </View>
                    </View>
                    <Entypo name="pencil" 
                            style={styles.update} 
                            onPress={() => navigation.navigate('home')} 
                            size={25}
                    />
                </View>
            </View>
        </RecapLayout>		
    );
};

const styles = StyleSheet.create({
    hourglass : {
        justifyContent : 'center',
        alignSelf:  'center',
        width: 70,
        height : 70,
        marginBottom : 20,
    },
    highTitle : {
        marginVertical : 16,
        fontWeight : 'bold',
        fontSize : 18,
        color : '#4B4B4B',
    },
    miniTitle : {
        marginTop : 20,
        marginBottom : 10,
        fontWeight : 'bold',
        fontSize : 14,
        color : '#B2BEC3', 
    },
    recapContainer : {
        paddingHorizontal : 20
    },
    inSale : {
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'space-evenly'
    },
    condition : {
        display : 'flex', 
        justifyContent : 'space-around',
        flexDirection : 'row', 
        alignItems : 'center'
    },
    tagWrapper : {
        display : 'flex', 
        flexWrap :'wrap', 
        flexDirection:'row'
    },
    tag : {
        backgroundColor : '#2CBDF3',
        borderRadius : 15,
        display : 'flex',
        flexDirection : 'row',
        alignContent : 'center',
        justifyContent : 'space-between',
        paddingVertical : 5,
        paddingHorizontal : 20,
        margin : 2,
        color : '#fff',
        fontSize : 14
    },
    image : {
        borderRadius : 15,
        height : 80,
        width : 80,
    },
    filledBottle : {
        position : 'relative',
    },
    bottle : {
        width: 80,
        height : 100,
        position : 'relative'
    },   
    water : {
        position : 'absolute',
        width: '100%',
        bottom : 0
    },
    price : {
        alignSelf : 'center',
        paddingHorizontal : 40,
        paddingVertical : 10,
        borderRadius : 100,
        borderWidth : 1,
        marginHorizontal : 30,
    },
    update : {
        position :'absolute',
        top : 10,
        right : 0,
        opacity : 0.1
    }
})

const mapStateToProps = state => {
    return { proposal : state.form.proposalForm,
             ads : state.adReducer,
             authenticate : state.authReducer
            }
}

const mapDispatchToProps = (dispatch) => {
    const dispatchesProps = {
          createAd : adsActions.createAdAction
    };
    return bindActionCreators(dispatchesProps, dispatch);
}

ProposalRecap = reduxForm({ form : "proposalForm",
                        destroyOnUnmount: false 
                      })(ProposalRecap)

export const ProposalRecapContainer = connect(mapStateToProps, mapDispatchToProps)(ProposalRecap)