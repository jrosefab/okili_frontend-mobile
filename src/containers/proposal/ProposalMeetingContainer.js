import React from 'react';
import { Field, reduxForm, isDirty } from 'redux-form';
import { Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import {StepLayout, DateTimePickerRender } from '../../components';
import Clock from '../../assets/illustrations/clock.png';

function ProposalMeeting( props ){
    const  { ads, navigation, conditionFilled } = props;

    return (
        <StepLayout navigation={navigation} 
                    isProposition={true}
                    position={0} 
                    goForward={true}
                    message={"un ou plusieurs champs(*) contiennent une erreur"}
                    nextScreen={'proposal_offer'}
                    illustration={Clock}
        >
            <Text style={styles.highTitle}>Proposer un rendez-vous*</Text>
            <Field name="meeting" component={DateTimePickerRender}/>
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    },
    inputContainer:{
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'center',
        alignContent :'center',
        alignItems :'center'
    },  
    slider : { 
        position: "absolute",
        zIndex : 5,
    },
    filledBottle : {
        position : 'relative'
    },
    bottle : {
        width: 120,
        height : 140,
        position : 'relative'
    },   
    water : {
        position : 'absolute',
        width: '100%',
        bottom : 0
    },
    camera : {
        borderRadius : 20,
        height : 80,
        width : 80,
        backgroundColor : '#E4E4E4'
    },
})

const mapStateToProps = state => {
    return { 
        ads : state.adsReducer,
        proposal : state.form.proposalForm,
        quantityFilled:isDirty('proposalForm')(state, ["quantity_left"]), 
        pictureFilled:isDirty('proposalForm')(state, ["picture_proposal"])
    };
};

ProposalMeeting = reduxForm({ form : "proposalForm",
                                initialValues : { 
                                    category: null,
                                    kind : "research",
                                    home_delivery : false,
                                    home_pickup : true,
                                    home_pickup_details : null,
                                    delivery_km_acceptation : 0,
                                    condition : null,
                                    quantity_left : null
                                },
                            destroyOnUnmount: false,
                        })(ProposalMeeting);

export const ProposalMeetingContainer = connect(mapStateToProps, null)(ProposalMeeting);