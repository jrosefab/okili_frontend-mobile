import React, { useEffect, useState } from 'react';
import moment from 'moment';
import 'moment/locale/fr';
import { bindActionCreators } from 'redux';
import { Text, View, StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { adsActions } from '../store/actions/ads.actions';
import { AdListedCard,  MyAdListLayout } from '../components'
import { REACT_APP_OKILI_BACK } from 'react-native-dotenv';


function MyAdList(props){
    const { authenticate, user, ads, navigation } = props
    const [ myAdList, setMyAdList ] = useState([]);
    const [ isLogged, setIsLogged ] = useState(false);

    const getCreatedDate = (data) => (
        moment(data).locale('fr').startOf('hour').fromNow()
    );
    
    const truncateString = (str) => {
        return str.length > 30 ? str.substring(0, 34) + "..." : str;
    }

    const fetchMyAdList = async(profileId) => {
        await props.getMyAdList(profileId)
    }

    const fetchSingleAd = (adId) => {
        props.getSingleAd(adId)
    }

    useEffect(() => {
        if (!authenticate.profileId){
            setIsLogged(false)
        }else{
            setIsLogged(true)
            fetchMyAdList(authenticate.profileId);
        }
    }, [authenticate])

    useEffect(() => {
        if (ads.myAdList !== myAdList){
            setMyAdList(ads.myAdList);
        }
    }, [ads.myAdList])


    const renderAdList = (item) => (
        <AdListedCard getAd={() => {
                fetchSingleAd(item._id)
                navigation.push('single_ad')
        }}
                title={item.title}
                item={item}
                key={item._id}
                dateTime={getCreatedDate(item.created_date)}
                description={item.details_content}
                picture={`${REACT_APP_OKILI_BACK}/${item.pictures[0].source}`}
                haveProposals={ item.proposals.length === 0 ? false : true } 
                //addToBasket={ ajouter l'id a la route achat }
                //sendProposal={ ajouter l'id a la route de proposition }
        />
    )
    return (
        <MyAdListLayout>
            {!isLogged ?
                <View style={styles.is_logged}>
                    <Text>Aucun contenu</Text>
                </View>
            :
            myAdList && 
                myAdList.length === 0 ?
                    <View>
                        <Text>Vous n'avez aucun annonces en cours</Text>
                        <Button title="Outline button"
                                type="outline"
                        />
                    </View>
                :
                <FlatList style={{ marginBottom : 50 }}
                        data={myAdList}
                        horizontal={false}
                        numColumns={2}
                        renderItem={({ item })=>
                            renderAdList(item)
                        }
                        keyExtractor={(item, index) => index.toString()}
                />
            }
        </MyAdListLayout>
    )
}

const styles = StyleSheet.create({
    is_logged : {
       height : 200,
    }
});
const mapStateToProps = (state) => {
    return {
        ads : state.adsReducer,
        authenticate : state.authReducer,
        user : state.userReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const dispatchesProps = {
        getMyAdList : adsActions.getMyAdListAction,
        getSingleAd : adsActions.getSingleAdAction
    };
    return bindActionCreators(dispatchesProps, dispatch);
}

export const MyAdListContainer = connect(mapStateToProps, mapDispatchToProps)(MyAdList)
