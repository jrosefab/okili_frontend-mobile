import React, { useEffect } from 'react';
import { Field, change, reduxForm, isDirty } from 'redux-form';
import { View,Image, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import {StepLayout, RadioRender, 
        VerticalSliderRender, ImagePickerRender} from '../../../components';
import Condition from '../../../assets/illustrations/condition.png';
import Bottle from '../../../assets/illustrations/bottle.png';
import Water from '../../../assets/illustrations/water.png';
import Camera from '../../../assets/illustrations/camera.png';

function RequestCondition( props ){
    const  { navigation, conditionFilled, condition,
             category, quantityFilled, quantity_left, picture } = props;

             console.log(category)
    
    const clearField = () => {
        const quantityLeftField = "quantity_left";
        const conditionField = "condition";

        if (category === "equipment"){
            if (quantity_left !== null);
                props.dispatch(change("createRequestForm", quantityLeftField, null ))
        }else{
            if (condition !== null);
                props.dispatch(change("createRequestForm", conditionField, null ))
        };
    };

    useEffect(() =>{
        clearField();
     }, [] )

    return (
        <StepLayout navigation={navigation} 
                    position={2} 
                    goForward={conditionFilled || quantityFilled ? true : false }
                    message={"un ou plusieurs champs(*) contiennent une erreur"}
                    nextScreen={'request_urgency'}
                    illustration={Condition}
        >
            <View>
                <Text style={styles.highTitle}>Quel état accepteriez vous ?*</Text>
                <Field name="condition"
                        radios={[{ label : 'Neuf', value:'new'},
                                 { label : 'Déjà utilisé', value:'used'}, 
                                 { label : 'Les deux', value:'both'}
                                ]}
                        component={RadioRender}
                />
            </View>
            <Text style={styles.highTitle}>Ajouter une photo*</Text>
            <Field style={styles.camera} 
                   defaultImage={Camera} 
                   name="picture_publication" 
                   source={ picture && { uri : picture.uri } } 
                   component={ImagePickerRender}
            />
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    },
    inputContainer:{
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'center',
        alignContent :'center',
        alignItems :'center'
    },  
    slider : { 
        position: "absolute",
        zIndex : 5,
    },
    filledBottle : {
        position : 'relative'
    },
    bottle : {
        width: 120,
        height : 140,
        position : 'relative'
    },   
    water : {
        position : 'absolute',
        width: '100%',
        bottom : 0
    },
    camera : {
        borderRadius : 20,
        height : 80,
        width : 80,
        backgroundColor : '#E4E4E4'
    },
})

const mapStateToProps = state => {
    return { category : state.form.createRequestForm.values.category,
             picture : state.form.createRequestForm.values.picture_publication,
             condition : state.form.createRequestForm.values.condition,
             quantity_left : state.form.createRequestForm.values.quantity_left,
             currentCondition : state.form.createRequestForm.values.current_condition,
             conditionFilled:isDirty('createRequestForm')(state, ["condition"]), 
             quantityFilled:isDirty('createRequestForm')(state, ["quantity_left"]), 
             pictureFilled:isDirty('createRequestForm')(state, ["picture_publication"])
    };
};

RequestCondition = reduxForm({ form : "createRequestForm",
                            destroyOnUnmount: false,
                        })(RequestCondition);

export const RequestConditionContainer = connect(mapStateToProps, null)(RequestCondition);