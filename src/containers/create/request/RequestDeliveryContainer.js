import React, { useEffect} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, change, 
         formValueSelector, isDirty } from 'redux-form';
import { View, YellowBox, Text, 
         StyleSheet } from 'react-native';
import { getCurrentLocationAction, 
         getAddressFromCoordinatesAction } from '../../../store/actions/geolocation.actions';
import {StepLayout, BoxSelectRender, 
        TextRender, HorizontalSliderRender, 
        GeoLocationRender } from '../../../components';
import Box from '../../../assets/illustrations/box.png';

YellowBox.ignoreWarnings([
    'VirtualizedLists should never be nested',
    'If you are using React Native',
])

function RequestDelivery( props ){
    const  { geoLocation, navigation, home_pickup, 
             home_delivery, defaultGeolocation, 
             accessDetailsFilled, acceptationKmFilled } = props;

    /*if user want to change, these function remove the no needed fields for schema*/
    const clearField = () => {
        const homeDelivery          = "home_delivery";
        const deliveryKmAcceptation = "delivery_km_acceptation";
        const homepPikup            = "home_pickup";
        const homePickupDetails     = "home_pickup_details";

        if (home_pickup === false){
            props.dispatch(change("createRequestForm", homePickupDetails, null ));
            props.dispatch(change("createRequestForm", home_pickup, false ));
        }
        if (home_delivery === false){
            props.dispatch(change("createRequestForm", deliveryKmAcceptation, 0 ));
            props.dispatch(change("createRequestForm", home_delivery, false ));
        }
    };
    
    useEffect(() => {
        props.getCurrentLocation();
    }, [] );
    
    useEffect(() => {
        const fetchAddress = async() => {
            const latitude = props.geoLocation.currentLocation.coords.latitude;
            const longitude = props.geoLocation.currentLocation.coords.longitude;
            await props.getAddressFromCoordinates(latitude, longitude);
        };
        fetchAddress();
    }, [props.geoLocation] );

    
    return (
        <StepLayout navigation={navigation} 
                    position={4} 
                    goForward={ !defaultGeolocation ? false : 
                                     defaultGeolocation.longitude === 0 &&
                                     defaultGeolocation.latitude  === 0 ? false :
                                    /* If both delivery mode is clicked, then check if all field is dirty */
                                   ( home_delivery !== false &&  
                                           home_pickup !== false ) ? 
                                            acceptationKmFilled &&
                                            accessDetailsFilled ? true : false
                                       /* If home_pickup mode is clicked, then check if all field is dirty and location !== 0 */
                                        :  home_pickup !== false ?
                                                acceptationKmFilled ? true : false

                                         : home_delivery !== false ?
                                                accessDetailsFilled ? true : false
                                : false
                    }
                    message={"un ou plusieurs champs(*) n'est pas remplit"}
                    nextScreen={'request_price'}
                    illustration={Box}
        >
            <Text style={styles.highTitle}>Quel mode de livraison préférez-vous ?*</Text>
            <View style={styles.delivery_wrapper}>
                <Field name="home_pickup" 
                       box={{ value : !home_pickup ? true : false,
                              background : '#F19CB8' , 
                              icon : 'home',
                              text : 'Je récupère',
                              opacity : home_pickup ? 1 : 0.5 }} 
                       component={BoxSelectRender}
                       onChange={() => clearField()}
                />
                <Field name="home_delivery" 
                       box={{ value : !home_delivery ? true : false,
                              background : '#FFB25A' , 
                              icon : 'truck',
                              text : 'On me livre',
                              opacity : home_delivery ? 1 : 0.5 }} 
                       component={BoxSelectRender}
                       onChange={() => clearField()}
                />
            </View>
            <View>
                <Text style={styles.highTitle}>Localisation actuelle*</Text>
                <Field actualLocation={{ 
                            address   : !geoLocation.currentLocation ? "" : geoLocation.address,
                            longitude : !geoLocation.currentLocation ? 0  : geoLocation.currentLocation.coords.longitude, 
                            latitude  : !geoLocation.currentLocation ? 0  : geoLocation.currentLocation.coords.latitude 
                        }}
                        name="geolocation"
                        component={GeoLocationRender}
                        IslocationAvailable={!geoLocation.currentLocation ? false : true }
                />
            </View>

            {home_delivery && 
                <View>
                    <Text style={styles.highTitle}>Détails d'accès au logement*</Text>
                    <Field name="home_pickup_details" component={TextRender}/>
                </View>
            }

            {home_pickup && 
                <View>
                    <Text style={styles.alert}>
                        Vous acceptez de vous déplacer dans un rayon de* :
                    </Text>
                    <Field name="delivery_km_acceptation"  
                           component={HorizontalSliderRender}
                    />
                </View>
            }
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    },
    delivery_wrapper : {
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'space-around',
        marginBottom : 10
    },
    delivery : {
        display : 'flex',
        justifyContent: 'center',
    },
    picto : {
        borderRadius : 15,
        marginVertical : 2
    },
    text : {
        alignSelf : 'center', 
        fontSize : 20,
        color : '#717171'
    },
    alert : {
        textAlign : 'justify',
        marginVertical : 20,
        marginHorizontal : 20,
    }
});

const selector = formValueSelector('createRequestForm')
const mapStateToProps = state => {
    return { 
        geoLocation : state.geoLocationReducer,
        home_pickup : state.form.createRequestForm.values.home_pickup,
        home_pickup_details : state.form.createRequestForm.values.home_pickup_details,
        home_delivery : state.form.createRequestForm.values.home_delivery,
        delivery_km_acceptation : state.form.createRequestForm.values.delivery_km_acceptation,

        deliveryFilled:isDirty('createRequestForm')(state, ["home_delivery"]),
        acceptationKmFilled:isDirty('createRequestForm')(state, ["delivery_km_acceptation"]),
        accessDetailsFilled:isDirty('createRequestForm')(state, ["home_pickup_details"]),
        geoLocationFilled:isDirty('createRequestForm')(state, ["geolocation"]),
        defaultGeolocation:selector(state, 'geolocation')
    };
};

const mapDispatchToProps = (dispatch) => {
    const dispatchesProps = {
          getCurrentLocation : getCurrentLocationAction,
          getAddressFromCoordinates : getAddressFromCoordinatesAction
    };
    return bindActionCreators(dispatchesProps, dispatch);
};

RequestDelivery = reduxForm({ form : "createRequestForm",
                            destroyOnUnmount: false 
                        })(RequestDelivery);

export const RequestDeliveryContainer = connect(mapStateToProps, mapDispatchToProps)(RequestDelivery);