import React from 'react';
import { Field, reduxForm, isDirty } from 'redux-form';
import { connect } from 'react-redux';
import { Text, StyleSheet } from 'react-native';
import { StepLayout, TagsRender, TextRender } from '../../../components';
import Infos from '../../../assets/illustrations/infos.png';

function RequestInfos(props){
    const { navigation, tagsArray, titleFilled, detailsFilled} = props;
    const defaultTags = ["café"]
    return (
        <StepLayout navigation={navigation}
                    position={1}
                    goForward={titleFilled && detailsFilled ? true : false }
                    message={"un ou plusieurs champs(*) contiennent une erreur"}
                    nextScreen={'request_condition'}
                    illustration={Infos}
        >
            <Text style={styles.highTitle}>Choisir un titre*</Text>
            <Field name="title" component={TextRender} placeholder="Ex : Capusle pour Nespresso."/>

            <Text style={styles.highTitle}>Tags produits 
            <Text style={{ fontWeight : 'normal', fontSize : 8 }}>( Espace pour valider )</Text></Text>
            {tagsArray && tagsArray.values && 
                <Field name="tags" 
                       component={TagsRender}
                       initialTags={!tagsArray.values.tags ? 
                                    defaultTags : tagsArray.values.tags }
                />
            }
            <Text style={styles.highTitle}>Détails complémentaire*</Text>
            <Field name="details_content" component={TextRender} placeholder="Descriptif ou informations nécessaire sur votre bien."/>
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    }
})

const mapStateToProps = state => {
    return { tagsArray : state.form.createRequestForm,
             titleFilled:isDirty('createRequestForm')(state, ["title"]),
             detailsFilled:isDirty('createRequestForm')(state, ["details_content"]), 
            };
};

RequestInfos = reduxForm({ form : "createRequestForm",
                           initialValues : { 
                               category: null,
                               kind : "research",
                               home_delivery : false,
                               home_pickup : true,
                               home_pickup_details : null,
                               delivery_km_acceptation : 0,
                               condition : null,
                               quantity_left : null
                           },
                        destroyOnUnmount: false
                     })(RequestInfos)

export const RequestInfosContainer = connect(mapStateToProps, null)(RequestInfos)