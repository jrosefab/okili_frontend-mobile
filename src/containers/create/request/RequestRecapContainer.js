import React, {useState} from 'react';
import { reduxForm, reset } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Text, View, StyleSheet, 
         Image, Alert} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import { adsActions } from '../../../store/actions/ads.actions';
import { RecapLayout, Map, Loading, ModalLogin } from '../../../components';
import HourGlass from '../../../assets/illustrations/hourglass.png';

function RequestRecap(props){
    const { authenticate, ad, navigation, handleSubmit } = props;
    const [modalVisible, setModalVisible] = useState(false)

    const submitAd = (ad) => {
       if(!authenticate.profileId){
           setModalVisible(true) 
       }else{
           props.createAd(ad)
           navigation.navigate('home'); 
           props.dispatch(reset('createRequestForm'))
       }
    }
    const deleteFormAd = () => {
        Alert.alert(
            'Attention',
            'Vous êtes sur le point de supprimer votre annonce', // <- this part is optional, you can pass an empty string
            [
                {text: 'Annuler', onPress: () => { setModalVisible(false)}, style: 'cancel' },
                {text: 'Supprimer', onPress: () => {
                    navigation.navigate('home'); 
                    props.dispatch(reset('createRequestForm'));
                }},
            ],
            {cancelable: true},
        );
    };
    
    return (
        <RecapLayout 
            onSubmit={() => submitAd(ad.values)}
            onCancel={() => deleteFormAd()}
        >
        {ad ? ad.values&& ad.values.geolocation &&
            <>
                <Text style={styles.highTitle}>Votre recherche est en attente de propositions</Text>
                <Image source={HourGlass} style={styles.hourglass}/>
                <Text style={[styles.highTitle, { marginBottom : 0 }]}>Récapitulatif :</Text>
                <View style={styles.recapContainer}>
                    
                    <View style={{ position : 'relative' }}>
                        <Text style={styles.miniTitle}>Vous recherchez :</Text>
                        <View style={styles.inSale}>
                            <View style={{ paddingHorizontal : 20, flexShrink : 1 }}>
                                <Text style={{ fontSize : 18, alignSelf: 'center', marginBottom : 5 }}>{ad.values.title}</Text>
                                <View style={styles.tagWrapper}>
                                    {!ad.values.tags ? null : 
                                    ad.values.tags.map( tag => (
                                        <Text key={tag} style={styles.tag}>{tag}</Text>
                                    ))}
                                </View>
                            </View>
                        </View>
                        <Entypo name="pencil" 
                                style={styles.update} 
                                onPress={() => navigation.navigate('request_category')} 
                                size={25}
                        />
                    </View>

                    <View style={{ position : 'relative' }}>
                        <Text style={styles.miniTitle}>États général</Text>
                        <View style={styles.condition}>
                            <Image style={styles.image}  source={ad.values.picture_publication}/>
                            { ad.values.condition === 'new' ?
                                <Text>Produit neuf.</Text>
                            : ad.values.condition === 'used' &&
                                <Text>Produit déjà utilisé.</Text>
                            }
                        </View>
                        <Entypo name="pencil" 
                            style={styles.update} 
                            onPress={() => navigation.navigate('request_condition')} 
                            size={25}
                        />
                    </View>

                    <View style={{ position : 'relative' }}>
                        <Text style={styles.miniTitle}>Livraison :</Text>
                        { ad.values.home_delivery && 
                            <>
                                <Text>
                                    {`Article a livrer a l'adresse :\n${ad.values.geolocation.address}\n`}
                                </Text>
                                <Text>
                                    {!ad.values.home_pickup_details ? "Pas de détails d'accès à votre adresse.\n" 
                                    : `Détails d'accès à votre adresse : \n${ad.values.home_pickup_details}\n`}
                                </Text>
                            </>
                        }
                        { ad.values.home_pickup && 
                            <Text>
                                <Entypo name="location-pin" size={16}/>
                                Vous pouvez vous déplacer jusqu'à {ad.values.delivery_km_acceptation}km 
                            </Text> 
                        }
                        <Entypo name="pencil" 
                                style={styles.update} 
                                onPress={() => navigation.navigate('request_delivery')} 
                                size={25}
                        />
                    </View>

                    <View style={{ position : 'relative' }}>
                        <Map region={{ 
                                latitude: ad.values.geolocation.latitude,
                                longitude: ad.values.geolocation.longitude, 
                                latitudeDelta: 0.009,
                                longitudeDelta: 0.009,
                            }}
                            coordinateMark={{ 
                                latitude: ad.values.geolocation.latitude,
                                longitude: ad.values.geolocation.longitude 
                            }}
                        />
                    </View>

                    <View style={{ position : 'relative' }}>
                        <Text style={styles.miniTitle}>Vous recevrez dès validation : </Text>
                        <View style={styles.price}>
                            <Text style={{ fontSize : 20 }}>{ad.values.price} €</Text>
                        </View>
                        <Entypo name="pencil" 
                                style={styles.update} 
                                onPress={() => navigation.navigate('request_price')} 
                                size={25}
                        />
                    </View>
                </View>
                {/* modal */}
                <ModalLogin 
                        visible={modalVisible}
                        onPress={() => {
                            setModalVisible(!modalVisible);
                        }}
                        onRequestClose={!modalVisible}
                        login={()=>{ 
                            navigation.navigate('login');
                            setModalVisible(false)}
                        }
                        signup={()=>{ 
                            navigation.navigate('login', { screen : 'signup' });
                            setModalVisible(false)
                        }}
                />
            </>
            :
            <Loading/>
        }
        </RecapLayout>		
    );
};

const styles = StyleSheet.create({
    hourglass : {
        justifyContent : 'center',
        alignSelf:  'center',
        width: 70,
        height : 70,
        marginBottom : 20,
    },
    highTitle : {
        marginVertical : 16,
        fontWeight : 'bold',
        fontSize : 18,
        color : '#4B4B4B',
    },
    miniTitle : {
        marginTop : 20,
        marginBottom : 10,
        fontWeight : 'bold',
        fontSize : 14,
        color : '#B2BEC3', 
    },
    recapContainer : {
        paddingHorizontal : 20
    },
    inSale : {
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'space-evenly'
    },
    condition : {
        display : 'flex', 
        justifyContent : 'space-around',
        flexDirection : 'column', 
        alignItems : 'center'
    },
    tagWrapper : {
        display : 'flex', 
        flexWrap :'wrap', 
        flexDirection:'row'
    },
    tag : {
        backgroundColor : '#2CBDF3',
        borderRadius : 15,
        display : 'flex',
        flexDirection : 'row',
        alignContent : 'center',
        justifyContent : 'space-between',
        paddingVertical : 5,
        paddingHorizontal : 20,
        margin : 2,
        color : '#fff',
        fontSize : 14
    },
    image : {
        borderRadius : 15,
        height : 80,
        width : 80,
    },
    filledBottle : {
        position : 'relative',
    },
    bottle : {
        width: 80,
        height : 100,
        position : 'relative'
    },   
    water : {
        position : 'absolute',
        width: '100%',
        bottom : 0
    },
    price : {
        alignSelf : 'center',
        paddingHorizontal : 40,
        paddingVertical : 10,
        borderRadius : 100,
        borderWidth : 1,
        marginHorizontal : 30,
    },
    update : {
        position :'absolute',
        top : 10,
        right : 0,
        opacity : 0.1
    }
})

const mapStateToProps = state => {
    return { ad : state.form.createRequestForm,
             userData : state.userReducer,
             authenticate : state.authReducer
            }
}

const mapDispatchToProps = (dispatch) => {
    const dispatchesProps = {
          createAd : adsActions.createAdAction
    };
    return bindActionCreators(dispatchesProps, dispatch);
}

RequestRecap = reduxForm({ form : "createRequestForm",
                        destroyOnUnmount: false 
                      })(RequestRecap)

export const RequestRecapContainer = connect(mapStateToProps, mapDispatchToProps)(RequestRecap)