import React from 'react';
import { Field, reduxForm, isDirty } from 'redux-form';
import { connect } from 'react-redux';
import { Text, StyleSheet } from 'react-native';
import { StepLayout, TagsRender, TextRender } from '../../../components';
import Infos from '../../../assets/illustrations/infos.png';

function SaleInfos(props){
    const  { navigation } = props
    const { tagsArray, titleFilled, detailsFilled} = props;
    const defaultTags = ["café"]
    return (
        <StepLayout navigation={navigation}
                    position={1}
                    goForward={titleFilled && detailsFilled ? true : false }
                    message={"un ou plusieurs champs(*) contiennent une erreur"}
                    nextScreen={'sale_condition'}
                    illustration={Infos}
        >
            <Text style={styles.highTitle}>Choisir un titre*</Text>
            <Field name="title" component={TextRender} placeholder="Ex : Capusle pour Nespresso."/>

            <Text style={styles.highTitle}>Tags produits 
            <Text style={{ fontWeight : 'normal', fontSize : 8 }}>( Espace pour valider )</Text></Text>
            <Field initialTags={!tagsArray ? defaultTags : tagsArray} name="tags" component={TagsRender}/>

            <Text style={styles.highTitle}>Détails complémentaire*</Text>
            <Field name="details_content" component={TextRender} placeholder="Descriptif ou informations nécessaire sur votre bien."/>
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    }
})

const mapStateToProps = state => {
    return { tagsArray : state.form.createSaleForm.values.tags,
             titleFilled:isDirty('createSaleForm')(state, ["title"]),
             detailsFilled:isDirty('createSaleForm')(state, ["details_content"]), 
            };
};

SaleInfos = reduxForm({ form : "createSaleForm",
                        destroyOnUnmount: false
                     })(SaleInfos)

export const SaleInfosContainer = connect(mapStateToProps, null)(SaleInfos)