import React from 'react';
import { Field, reduxForm, isDirty } from 'redux-form';
import { connect } from 'react-redux';
import { View, Text, StyleSheet } from 'react-native';
import {StepLayout, CurrencyRender } from '../../../components';
import Ticket from '../../../assets/illustrations/ticket.png';


function SalePrice( props ){
    const  { navigation, priceFilled } = props
    return (
        <StepLayout navigation={navigation} 
                    position={5} 
                    goForward={priceFilled ? true : false }
                    message={"Veuillez indiquer votre prix"}
                    nextScreen={'sale_recap'}
                    illustration={Ticket}
        >
            <Text style={styles.highTitle}>À quel prix ?</Text>
            <View>
                <Field name="price" component={CurrencyRender}/> 
                <Text style={styles.alert}>
                    Attention, le prix conseillé vous assure 
                    de conclure marché dans les meilleurs délais.
                </Text>
            </View>
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    },
    alert : {
        textAlign : 'justify',
        marginVertical : 20,
        marginHorizontal : 20
    }
})

const mapStateToProps = state => {
    return { priceFilled:isDirty('createSaleForm')(state, ["price"]),
    caca : state.form.createSaleForm
    }
}

SalePrice = reduxForm({ form : "createSaleForm",
                        destroyOnUnmount: false })
                        (SalePrice)

export const SalePriceContainer = connect(mapStateToProps, null)(SalePrice)
