import React, { useEffect } from 'react';
import { Field, change, reduxForm, isDirty } from 'redux-form';
import { View,Image, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import {StepLayout, RadioRender, 
        VerticalSliderRender, ImagePickerRender} from '../../../components';
import Condition from '../../../assets/illustrations/condition.png';
import Bottle from '../../../assets/illustrations/bottle.png';
import Water from '../../../assets/illustrations/water.png';
import Camera from '../../../assets/illustrations/camera.png';

function SaleCondition( props ){
    const  { navigation, conditionFilled, condition,
             category, quantityFilled, quantity_left, picture } = props;
    
    const clearField = () => {
        const quantityLeftField = "quantity_left";
        const conditionField = "condition";

        if (category === "equipment"){
            if (quantity_left !== null);
                props.dispatch(change("createSaleForm", quantityLeftField, null ))
        }else{
            if (condition !== null);
                props.dispatch(change("createSaleForm", conditionField, null ))
        };
    };

    useEffect(() =>{
        clearField();
     }, [] )

    return (
        <StepLayout navigation={navigation} 
                    position={2} 
                    goForward={conditionFilled || quantityFilled ? true : false }
                    message={"un ou plusieurs champs(*) contiennent une erreur"}
                    nextScreen={'sale_urgency'}
                    illustration={Condition}
        >
            {category === "consumable" ?
                <>
                    <Text style={styles.highTitle}>Saisir la quantité restante de votre bien ?*</Text>
                    <Text style={{ alignSelf : 'center', fontSize : 10, marginBottom : 20}}>
                        Appuyer sur la bouteille
                    </Text>
                    <View style={styles.inputContainer}>
                        <View style={styles.slider}>
                            <Field name="quantity_left" 
                                   custom={{ minTrack : 'transparent', 
                                             maxTrack : 'transparent'}} 
                                   component={VerticalSliderRender}
                            /> 
                        </View>
                        <View style={styles.filledBottle}>
                            <Image
                                style={styles.water}
                                source={Water}
                                style={[styles.water, { height : quantity_left === null ? 
                                                                '0%' : `${quantity_left}%` } 
                                      ]}
                            />
                            <Image
                                style={styles.bottle}
                                source={Bottle}
                            />
                        </View>
                    </View>
                </>
                :
                <View>
                    <Text style={styles.highTitle}>Quel est l'état de votre bien ?*</Text>
                    <Field name="condition"
                           radios={[{ label : 'Neuf', value:'new'},
                                    { label : 'Très bon état', value:'good'},
                                    { label : 'État moyen, neutre', value:'neutral'},
                                    { label : 'Mauvais état', value:'bad'},
                                    { label : 'Pour pièces', value:'hs'}, ]}
                           component={RadioRender}
                    />
                </View>
            }
            <Text style={styles.highTitle}>Ajouter une photo*</Text>
            <Field style={styles.camera} 
                   defaultImage={Camera} 
                   name="picture_publication" 
                   source={ picture && { uri : picture.uri } } 
                   component={ImagePickerRender}
            />
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    },
    inputContainer:{
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'center',
        alignContent :'center',
        alignItems :'center'
    },  
    slider : { 
        position: "absolute",
        zIndex : 5,
    },
    filledBottle : {
        position : 'relative'
    },
    bottle : {
        width: 120,
        height : 140,
        position : 'relative'
    },   
    water : {
        position : 'absolute',
        width: '100%',
        bottom : 0
    },
    camera : {
        borderRadius : 20,
        height : 80,
        width : 80,
        backgroundColor : '#E4E4E4'
    },
})

const mapStateToProps = state => {
    return { category : state.form.createSaleForm.values.category,
             picture : state.form.createSaleForm.values.picture_publication,
             condition : state.form.createSaleForm.values.condition,
             quantity_left : state.form.createSaleForm.values.quantity_left,
             currentCondition : state.form.createSaleForm.values.current_condition,
             conditionFilled:isDirty('createSaleForm')(state, ["condition"]), 
             quantityFilled:isDirty('createSaleForm')(state, ["quantity_left"]), 
             pictureFilled:isDirty('createSaleForm')(state, ["picture_publication"])
    };
};

SaleCondition = reduxForm({ form : "createSaleForm",
                            destroyOnUnmount: false,
                        })(SaleCondition);

export const SaleConditionContainer = connect(mapStateToProps, null)(SaleCondition);