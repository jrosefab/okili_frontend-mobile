import React from 'react';
import { Field, isDirty, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { View, Text, StyleSheet } from 'react-native';
import {StepLayout, RadioRender } from '../../../components';
import Timer from '../../../assets/illustrations/timer.png';

function SaleUrgency ( props ){
    const  { navigation, urgencyFilled } = props
    return (
        <StepLayout navigation={navigation} 
                    position={3} 
                    goForward={urgencyFilled ? true : false }
                    message={"Veuillez chosir l'urgence"}
                    nextScreen={'sale_delivery'}
                    illustration={Timer}
        >
            <Text style={styles.highTitle}>Quelle est l'urgence ?*</Text>
            <View>
                <Field radios={[{ label : 'Sur le champs ! Je suis pressé(e)',  value:'hurry'  },
                                { label : 'Rapidement serait bien.',            value:'fast'   },
                                { label : "J'attends, mais pas trop non plus.", value:'canWait'},
                                { label : 'Lentement mais surement.',           value:'slow'   } ]} 
                        name="urgency"
                        component={RadioRender}
                />
            </View>
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    }
})

const mapStateToProps = state => {
    return { urgencyFilled:isDirty('createSaleForm')(state, ["urgency"]),
    }
}

SaleUrgency = reduxForm({ form : "createSaleForm",
                          destroyOnUnmount: false })
                          (SaleUrgency)

export const SaleUrgencyContainer = connect(mapStateToProps, null)(SaleUrgency)
