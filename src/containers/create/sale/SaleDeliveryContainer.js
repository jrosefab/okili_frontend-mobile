import React, { useEffect} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, change, 
         formValueSelector, isDirty } from 'redux-form';
import { View, YellowBox, Text, 
         StyleSheet } from 'react-native';
import { getCurrentLocationAction, 
         getAddressFromCoordinatesAction } from '../../../store/actions/geolocation.actions';
import {StepLayout, BoxSelectRender, 
        TextRender, HorizontalSliderRender, 
        GeoLocationRender } from '../../../components';
import Box from '../../../assets/illustrations/box.png';
import Camera from '../../../assets/illustrations/camera.png';

YellowBox.ignoreWarnings([
    'VirtualizedLists should never be nested',
    'If you are using React Native',
])

function SaleDelivery( props ){
    const  { geoLocation, navigation, home_pickup, 
             home_delivery, defaultGeolocation, 
             accessDetailsFilled, acceptationKmFilled } = props;

    /*if user want to change, these function remove the no needed fields for schema*/
    const clearField = () => {
        const homeDelivery          = "home_delivery";
        const deliveryKmAcceptation = "delivery_km_acceptation";
        const homepPikup            = "home_pickup";
        const homePickupDetails     = "home_pickup_details";

        if (home_delivery === false){
            props.dispatch(change("createSaleForm", homeDelivery, false ));
            props.dispatch(change("createSaleForm", deliveryKmAcceptation, 0 ));
        }
        if (home_pickup === false){
            props.dispatch(change("createSaleForm", homePickupDetails, null ));
            props.dispatch(change("createSaleForm", homepPikup, false ));
        }
    };
    
    useEffect(() => {
        props.getCurrentLocation();
    }, [] );
    
    useEffect(() => {
        const fetchAddress = async() => {
            const latitude = props.geoLocation.currentLocation.coords.latitude;
            const longitude = props.geoLocation.currentLocation.coords.longitude;
            await props.getAddressFromCoordinates(latitude, longitude);
        };
        fetchAddress();
    }, [props.geoLocation] );

    
    return (
        <StepLayout navigation={navigation} 
                    position={4} 
                    goForward={ !defaultGeolocation ? false : 
                                     defaultGeolocation.longitude === 0 &&
                                     defaultGeolocation.latitude  === 0 ? false :
                                    /* If both delivery mode is clicked, then check if all field is dirty */
                                   ( home_delivery !== false &&  
                                           home_pickup !== false ) ? 
                                            acceptationKmFilled &&
                                            accessDetailsFilled ? true : false
                                       /* If home_pickup mode is clicked, then check if all field is dirty and location !== 0 */
                                        :  home_pickup !== false ?
                                                 accessDetailsFilled ? true : false

                                         : home_delivery !== false ?
                                                 acceptationKmFilled ? true : false
                                : false
                    }
                    message={"un ou plusieurs champs(*) n'est pas remplit"}
                    nextScreen={'sale_price'}
                    illustration={Box}
        >
            <Text style={styles.highTitle}>Quel mode de livraison préférez-vous ?*</Text>
            <View style={styles.delivery_wrapper}>
                <Field name="home_pickup" 
                       box={{ value : !home_pickup ? true : false,
                              background : '#F19CB8' , 
                              icon : 'home',
                              text : 'A retirer',
                              opacity : home_pickup ? 1 : 0.5 }} 
                       component={BoxSelectRender}
                       onChange={() => clearField()}
                />
                <Field name="home_delivery" 
                       box={{ value : !home_delivery ? true : false,
                              background : '#FFB25A' , 
                              icon : 'truck',
                              text : 'Je livre',
                              opacity : home_delivery ? 1 : 0.5 }} 
                       component={BoxSelectRender}
                       onChange={() => clearField()}
                />
            </View>
            <View>
                <Text style={styles.highTitle}>Localisation actuelle*</Text>
                <Field actualLocation={{ 
                            address   : !geoLocation.currentLocation ? "" : geoLocation.address,
                            longitude : !geoLocation.currentLocation ? 0  : geoLocation.currentLocation.coords.longitude, 
                            latitude  : !geoLocation.currentLocation ? 0  : geoLocation.currentLocation.coords.latitude 
                        }}
                        name="geolocation"
                        component={GeoLocationRender}
                        IslocationAvailable={!geoLocation.currentLocation ? false : true }
                />
            </View>

            {home_pickup && 
                <View>
                    <Text style={styles.highTitle}>Détails d'accès au logement*</Text>
                    <Field name="home_pickup_details" component={TextRender}/>
                </View>
            }

            {home_delivery && 
                <View>
                    <Text style={styles.alert}>
                        Vous acceptez de livrer autour de chez vous dans un rayon de* :
                    </Text>
                    <Field name="delivery_km_acceptation"  
                           component={HorizontalSliderRender}
                    />
                </View>
            }
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 16,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    },
    delivery_wrapper : {
        display : 'flex',
        flexDirection : 'row',
        justifyContent : 'space-around',
        marginBottom : 10
    },
    delivery : {
        display : 'flex',
        justifyContent: 'center',
    },
    picto : {
        borderRadius : 15,
        marginVertical : 2
    },
    text : {
        alignSelf : 'center', 
        fontSize : 20,
        color : '#717171'
    },
    alert : {
        textAlign : 'justify',
        marginVertical : 20,
        marginHorizontal : 20,
    }
});

const selector = formValueSelector('createSaleForm')
const mapStateToProps = state => {
    return { 
        geoLocation : state.geoLocationReducer,
        home_pickup : state.form.createSaleForm.values.home_pickup,
        home_pickup_details : state.form.createSaleForm.values.home_pickup_details,
        home_delivery : state.form.createSaleForm.values.home_delivery,
        delivery_km_acceptation : state.form.createSaleForm.values.delivery_km_acceptation,

        deliveryFilled:isDirty('createSaleForm')(state, ["home_delivery"]),
        acceptationKmFilled:isDirty('createSaleForm')(state, ["delivery_km_acceptation"]),
        accessDetailsFilled:isDirty('createSaleForm')(state, ["home_pickup_details"]),
        geoLocationFilled:isDirty('createSaleForm')(state, ["geolocation"]),
        defaultGeolocation:selector(state, 'geolocation')
    };
};

const mapDispatchToProps = (dispatch) => {
    const dispatchesProps = {
          getCurrentLocation : getCurrentLocationAction,
          getAddressFromCoordinates : getAddressFromCoordinatesAction
    };
    return bindActionCreators(dispatchesProps, dispatch);
};

SaleDelivery = reduxForm({ form : "createSaleForm",
                            destroyOnUnmount: false 
                        })(SaleDelivery);

export const SaleDeliveryContainer = connect(mapStateToProps, mapDispatchToProps)(SaleDelivery);