import React from 'react';
import { Field, reduxForm, isDirty } from 'redux-form';
import { connect } from 'react-redux';
import { View, Text, StyleSheet } from 'react-native';
import { StepLayout, RadioRender } from '../../../components';
import Category from '../../../assets/illustrations/category.png';

function SaleCategory(props){
    const  { navigation, categoryFilled } = props
    return (
        <StepLayout navigation={navigation}
                    position={0}
                    goForward={categoryFilled ? true : false }
                    message={"Veuillez choisir une catégorie"}
                    nextScreen={ 'sale_infos'}
                    illustration={Category}
        >
            <Text style={styles.highTitle}>De quoi s'agit-il ?*</Text>
            <View>
                <Field radios={[{ label : 'Alimentaire & consommable', value:'consumable'},
                                { label : 'Matériel, objet et divers', value:'equipment'} ]} 
                       name="category"
                       component={RadioRender}
                />
            </View>
        </StepLayout>
    );
};

const styles = StyleSheet.create({
    highTitle : {
      marginVertical : 15,
      fontWeight : 'bold',
      fontSize : 18,
      color : '#4B4B4B',
    }
});

const mapStateToProps = state => {
    return { fields : state.form.createSaleForm,
            categoryFilled:isDirty('createSaleForm')(state, ["category"]),
    }
}

SaleCategory = reduxForm({ form : "createSaleForm",
                           initialValues : { 
                                kind : "shopping",
                                home_delivery : false,
                                home_pickup : true,
                                home_pickup_details : null,
                                delivery_km_acceptation : 0,
                                quantity_left : null,
                                condition : null,
                            },
                            destroyOnUnmount: false
                })(SaleCategory)

export const SaleCategoryContainer = connect(mapStateToProps, null)(SaleCategory)