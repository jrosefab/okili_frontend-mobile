import React, { useEffect, useState } from 'react';
import moment from 'moment';
import 'moment/locale/fr';
import { bindActionCreators } from 'redux';
import { Text, View, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { adsActions } from '../store/actions/ads.actions';
import { AdCard, Loading, RecentAdsLayout } from '../components'
import { REACT_APP_OKILI_BACK } from 'react-native-dotenv';


function RecentAds(props){
    const { navigation } = props
    const [ recentAds, setRecentAds ] = useState([]);

    const getCreatedDate = (data) => (
        moment(data).locale('fr').startOf('hour').fromNow()
    );
    
    const truncateString = (str) => {
        return str.length > 30 ? str.substring(0, 34) + "..." : str;
    }

    const fetchRecentAds = async() => {
        await props.getRecentAds()
    }

    const fetchSingleAd = (adId) => {
        props.getSingleAd(adId)
    }

    useEffect(() => {
        fetchRecentAds(); //check the result here
       // setInterval(fetchRecentAds, 50000)
    }, [])

    useEffect(() => {
        if (props.ads.recentAds !== recentAds){
            setRecentAds(props.ads.recentAds);
        }
    }, [props.ads.recentAds])  /* Si cette valeur est différente, la fonction est appelé */



    const renderMyAds = (item) => (
        <AdCard getAd={() => {                 
            fetchSingleAd(item._id)
            navigation.push('single_ad')
        }}
                title={item.title}
                item={item}
                key={item._id}
                dateTime={getCreatedDate(item.created_date)}
                description={truncateString(item.details_content)}
                picture={`${REACT_APP_OKILI_BACK}/${item.pictures[0].source}`}
                //location={ad.location}
                conditionType={item.current_condition.condition === null ? "prescription-bottle" : "certificate" }
                value={ item.current_condition.condition === null ? 
                       `${item.current_condition.quantity_left} %` 
                        : item.current_condition.condition }
                urgency= {  
                      item.urgency === 'hurry' ?
                        "Rapide"
                    : item.urgency === 'fast' ?
                        "Vite"
                    : item.urgency === 'canWait' ?
                        "Normal"
                    : item.urgency === 'slow' &&
                        "Lent"
                }
                price={`${item.price} €`}
                color={item.kind === "shopping" ? "#76C14B" : "#FF7D86" }
                isShopping={ item.kind === "shopping" ? true : false } 
                //addToBasket={ ajouter l'id a la route achat }
                //sendProposal={ ajouter l'id a la route de proposition }
        />
    )
    return (
        <RecentAdsLayout>
            {recentAds && 
            recentAds.length === 0 ?
               <Loading/>
            :
            <FlatList style={{ marginBottom : 50 }}
                      data={recentAds}
                      horizontal={false}
                      numColumns={2}
                      renderItem={({ item })=>
                        renderMyAds(item)
                      }
                      keyExtractor={(item, index) => index.toString()}
              />
            }
        </RecentAdsLayout>
    )

}

const mapStateToProps = (state) => {
    return {
        ads : state.adsReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    const dispatchesProps = {
        getRecentAds : adsActions.getRecentAdsAction,
        getSingleAd : adsActions.getSingleAdAction
    };
    return bindActionCreators(dispatchesProps, dispatch);
}

export const RecentAdsContainer = connect(mapStateToProps, mapDispatchToProps)(RecentAds)
