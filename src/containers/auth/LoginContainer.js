import React, { useState }from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, isDirty } from 'redux-form';
import { authActions } from '../../store/actions/auth.actions';
import { AuthLayout, TextRender, EmailRender } from '../../components';
import { View, StyleSheet, Text, Button } from 'react-native';

function Login(props){
    const {navigation, authenticate, handleSubmit} = props;
    const [error, setError] = useState("")

    const submitUser = (user) => {
        props.login(user)
    }

    return (
        <AuthLayout authMode="Se connecter"
                     authMethodText="Vous n'avez pas de compte ?"
                     authLink="Créer mon compte"
                     authPage={()=>navigation.push('signup')}
                     isScrollable={false}
                     onSubmit={handleSubmit(submitUser)}

        >
            <Text style={styles.error}>{authenticate && authenticate.error}</Text>
            <View style={styles.field}>
                <Field name="email" placeholder="Votre adresse email" component={EmailRender}/>
            </View>
            <View style={styles.field}>
                <Field secureTextEntry={true} name="password" placeholder="Mot de passe" component={TextRender}/>
                <Text style={styles.forgotten}>Mot de passe oublié</Text>
            </View>
        </AuthLayout>
    )
}

const styles = StyleSheet.create({
    field : {
        marginVertical : 4,
    },
    forgotten:{
        textAlign : 'right',
        margin : 4,
        color : '#B2BEC3' 
    },
    error : {
        color : '#FF7D86',
        alignSelf : 'center',
    }
});

const mapStateToProps = (state) => {
    return {
        user : state.userReducer,
        authenticate : state.authReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const dispatchesProps = {
        login : authActions.loginAction,
    };
    return bindActionCreators(dispatchesProps, dispatch);
}

Login = reduxForm({ form : "loginForm",
                    destroyOnUnmount: false })(Login)

export const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login);