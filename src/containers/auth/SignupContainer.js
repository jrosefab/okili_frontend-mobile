import React, { useEffect }from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm, change } from 'redux-form';
import { authActions } from '../../store/actions/auth.actions';
import { AuthLayout, ImagePickerRender, 
        GooglePlacesAutoRender, TextRender, 
        CircleLoader, EmailRender} from '../../components';
import { View, StyleSheet, TextInput } from 'react-native';
import DefaultUser from '../../assets/illustrations/default_picture.png';
import Entypo from 'react-native-vector-icons/Entypo';

function Signup(props){
    const { navigation, handleSubmit, userData } = props

    const clearField = () => {
        const userPicture = "picture_user";
        props.dispatch(change("signupForm", userPicture, null ));
    }

    const submitNewUser = (newUser) => {
        props.signup(newUser)
    }

    return (
        <AuthLayout authMode="S'inscrire"
                    authMethodText="Vous avez déjà un compte ?"
                    authLink="Se connecter"
                    authPage={() => navigation.navigate('login')}
                    onSubmit={handleSubmit(submitNewUser)}
        >
        
            <View style={[styles.field, {alignSelf : 'center', position : 'relative', marginBottom : 20 }]}>
            
            { userData &&
                <>
                    <Field style={styles.profile} 
                        defaultImage={DefaultUser} 
                        name="picture_user" 
                        image={ userData.values && 
                                userData.values.picture_user && 
                                { uri : userData.values.picture_user.uri } 
                        } 
                        component={ImagePickerRender}
                    />
                </>
            }

            </View>
            <View style={{ display : 'flex', flexDirection : 'row'}}>
                <View style={[styles.field, {width : '50%'}]}>
                    <Field name="firstname" placeholder="Prénom*" component={TextRender}/>
                </View>
                <View style={[styles.field, { width : '50%' }]}>
                    <Field name="lastname" placeholder="Nom*" component={TextRender}/>
                </View>
            </View>
            <View style={styles.field}>
                <Field name="email" placeholder="Votre adresse email*" component={EmailRender}/>
            </View>
            <View style={styles.field}>
                <Field name="password" placeholder="Mot de passe*" component={TextRender}/>
            </View>
            <View style={[styles.field, styles.textInput]}>
                <TextInput name="confirm_password" placeholder="Confirmer le mot de passe*" />
            </View>
            <View style={styles.field}>
                <Field name="phone_number" keyboardType={'numeric'} placeholder="Numéro de téléphone*" component={TextRender}/>
            </View>
            <View style={[styles.field, {zIndex : 99}]}>
                <Field name="address" placeholder="Adresse complète*" component={GooglePlacesAutoRender}/>
            </View>
        </AuthLayout>
    )
}

const styles = StyleSheet.create({
    field : {
        marginVertical : 4,
    },
    forgotten:{
        textAlign : 'right',
        margin : 4,
        color : '#B2BEC3' 
    },
    profile : {
        height : 120,
        width : 120,
        borderRadius : 300
    },
    add : {
        color : '#87D15C',
        position : 'absolute',
        bottom : 22,
        left : 105,
    },
    delete : {
        color : '#FF7D86',
        position : 'absolute',
        bottom : 0,
        left : 105,
        zIndex: 10,
    },
    textInput : {
        borderWidth : 1,
        borderColor : '#B2BEC3',
        borderRadius : 300,
        padding : 5,
        marginHorizontal : 3
    }
});

const mapStateToProps = (state) => {
    return {
        userData : state.form.signupForm,
        user : state.userReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    const dispatchesProps = {
        signup : authActions.signupAction,
    };
    return bindActionCreators(dispatchesProps, dispatch);
}

Signup = reduxForm({ form : "signupForm"  })(Signup)

export const SignupContainer = connect(mapStateToProps, mapDispatchToProps)(Signup);