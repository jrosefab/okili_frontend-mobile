import React, { useEffect} from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import BottomStackNavigator from './BottomStackNavigator';
import SaleStackNavigator from './SaleStackNavigator';
import RequestStackNavigator from './RequestStackNavigator';
import AuthStackNavigator from './AuthStackNavigator';
import UserStackNavigator from './UserStackNavigator';
import MyAdListStackNavigator from './MyAdListStackNavigator';
import SingleAdStackNavigator from './SingleAdStackNavigator';
import ProposalStackNavigator from './ProposalStackNavigator';
import { getUserAction } from '../store/actions/user.actions';
import { getCurrentLocationAction } from '../store/actions/geolocation.actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { HeaderWithLogo, PreviousArrow,
         HeaderCustom } from '../components';

const Stack = createStackNavigator();// créer une collection de page

function AppStack(props){
const { navigation } = props;
    useEffect(() => {
        props.getCurrentLocation();
    }, [] );

   /* useEffect(() =>{
        const fetchUserData = async() => {
            const id = props.authenticate.profileId;
            await props.getUser(id);
        }
        fetchUserData();
    }, [props.authenticate] )*/

    return (
        <Stack.Navigator initialRouteName="home">
            <Stack.Screen
                name="home"
                options={{ headerStyle : { backgroundColor: '#fff', 
                                        elevation: 0,
                                        shadowOffset: {
                                                height: 0 
                                        }},
                        headerTitle : props => <HeaderWithLogo onPress={ () => navigation.navigate("all_ads") } {...props} /> }}
                
                component={BottomStackNavigator}
            />

            <Stack.Screen
                name="my_adlist"
                options={{ headerStyle : { backgroundColor: '#fff', 
                                        elevation: 0,
                                        shadowOffset: {
                                                height: 0 
                                        }},
                        headerTitle : props => <HeaderWithLogo onPress={ () => navigation.navigate("my_adlist") } {...props} /> }}
                component={MyAdListStackNavigator}
            />

            <Stack.Screen
                name="single_ad"
                options={{ headerStyle : { backgroundColor: '#fff', 
                                        elevation: 0,
                                        shadowOffset: {
                                                height: 0 
                                        }},
                            animationEnabled: false,
                            headerShown : false,
                            headerLeft: (props) => 
                                <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                               
                            headerTitle : props => 
                                <HeaderCustom style={{ position : 'absolute'}} 
                                    currentStep={""} {...props} />,
                        }}
                component={SingleAdStackNavigator}
            />

            <Stack.Screen
                name="sale_category"
                options={{ headerStyle : { backgroundColor: '#fff', 
                                        elevation: 0,
                                        shadowOffset: { height: 0 }
                                        },
                            headerShown: false
                        }}
                component={SaleStackNavigator}
            />

            <Stack.Screen
                name="request_category"
                options={{ headerStyle : { backgroundColor: '#fff', 
                                        elevation: 0,
                                        shadowOffset: { height: 0 }
                                        },
                            headerShown: false
                        }}
                component={RequestStackNavigator}
            />

            <Stack.Screen
                name="proposal_meeting"
                options={{ headerStyle : { backgroundColor: '#fff', 
                                        elevation: 0,
                                        shadowOffset: { height: 0 }
                                        },
                            headerShown: false
                        }}
                component={ProposalStackNavigator}
            />

            <Stack.Screen
                name="profile"
                component={UserStackNavigator}
            />

            <Stack.Screen
                name="login"
                options={{ headerStyle : { backgroundColor: '#fff', 
                                           elevation: 0,
                                           shadowOffset: { height: 0 }
                                         },
                            headerShown: false
                        }}
                component={AuthStackNavigator}
            />
        </Stack.Navigator>
    )
};

const mapStateToProps = (state) => {
    return {
        user : state.userReducer,
        authenticate : state.authReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    const dispatchesProps = {
          getCurrentLocation : getCurrentLocationAction,
          getUser : getUserAction
    };
    return bindActionCreators(dispatchesProps, dispatch);
}

const AppStackNavigator = connect(mapStateToProps, mapDispatchToProps)(AppStack)
export default AppStackNavigator;