import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import AppStackNavigator from './AppStackNavigator';

const RootStack = createStackNavigator();

const RootStackNavigator = () => (
    <RootStack.Navigator>
        <RootStack.Screen
                name="App"
                options={{
                    headerShown: false,
                    cardStyle : { backgroundColor: '#FFFFFF' }
                }}
                component={AppStackNavigator}
            />
    </RootStack.Navigator>
)

export default RootStackNavigator