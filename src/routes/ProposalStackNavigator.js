
import React from 'react';
import {View, Button} from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import {ProposalOfferContainer, ProposalMeetingContainer,  
        ProposalPriceContainer, ProposalRecapContainer } from '../containers';
import { HeaderCustom, PreviousArrow, EllipseOptions } from '../components';

const ProposalStack = createStackNavigator();

const ProposalStackNavigator = ({ navigation }) => (
    <ProposalStack.Navigator initialRouteName="proposal_meeting">
        <ProposalStack.Screen name="proposal_meeting"
                          component={ProposalMeetingContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Rendez-vous"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
        <ProposalStack.Screen name="proposal_offer"
                              component={ProposalOfferContainer}
                              options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Ma proposition"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />

        <ProposalStack.Screen name="proposal_price"
                          component={ProposalPriceContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Prix"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />

        <ProposalStack.Screen name="proposal_recap"
                          component={ProposalRecapContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Récapitulatif"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
    </ProposalStack.Navigator>
)

export default ProposalStackNavigator; 