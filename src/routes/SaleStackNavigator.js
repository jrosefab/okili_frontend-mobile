
import React from 'react';
import {View, Button} from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import {SaleCategoryContainer, SaleInfosContainer,  
        SaleConditionContainer, SaleUrgencyContainer, 
        SaleDeliveryContainer, SalePriceContainer, 
        SaleRecapContainer } from '../containers';
import { HeaderCustom, PreviousArrow, EllipseOptions } from '../components';

const SaleStack = createStackNavigator();

const SaleStackNavigator = ({ navigation }) => (
    <SaleStack.Navigator initialRouteName="sale_category">
        <SaleStack.Screen name="sale_category"
                          component={SaleCategoryContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    },
                                     animationEnabled: false,
                                     headerTitle : props => 
                                        <HeaderCustom onPress={ () => navigation.goBack() } currentStep={"De quoi s'agit-il ?"} {...props} />, 
                                  }}
        />
        <SaleStack.Screen name="sale_infos"
                          component={SaleInfosContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Informations générales"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
        <SaleStack.Screen name="sale_condition"
                          component={SaleConditionContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Condition"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
        <SaleStack.Screen name="sale_urgency"
                          component={SaleUrgencyContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Urgence"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
        <SaleStack.Screen name="sale_delivery"
                          component={SaleDeliveryContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Livraison"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
        <SaleStack.Screen name="sale_price"
                          component={SalePriceContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                        <HeaderCustom style={{ position : 'absolute'}} currentStep={"Prix conseillé"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
        <SaleStack.Screen name="sale_recap"
                          component={SaleRecapContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Récapitulatif"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
    </SaleStack.Navigator>
)

export default SaleStackNavigator; 