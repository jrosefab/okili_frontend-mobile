import React from 'react';
import {View, Button} from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import {SaleCategoryContainer, SaleInfosContainer,  
        SaleConditionContainer, SaleUrgencyContainer, 
        SaleDeliveryContainer, SalePriceContainer, 
        SaleRecapContainer } from '../containers';
import { HeaderCustom, PreviousArrow, EllipseOptions } from '../components';

const UserStack = createStackNavigator();

const UserStackNavigator = ({ navigation }) => (
    <UserStack.Navigator initialRouteName="profile">
        <UserStack.Screen name="profile"
                          component={SaleCategoryContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    },
                                     animationEnabled: false,
                                     headerTitle : props => 
                                        <HeaderCustom onPress={ () => navigation.goBack() } currentStep={"De quoi s'agit-il ?"} {...props} />, 
                                  }}
        />
        <UserStack.Screen name="payment"
                          component={SaleInfosContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Informations générales"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
    </UserStack.Navigator>
)

export default UserStackNavigator; 