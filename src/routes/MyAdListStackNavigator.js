import React from 'react';
import {View, Button} from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import {MyAdListContainer } from '../containers';
import { HeaderCustom, PreviousArrow, EllipseOptions } from '../components';

const MyAdListStack = createStackNavigator();

const MyAdListStackNavigator = ({ navigation }) => (
    <MyAdListStack.Navigator initialRouteName="my_adlist">
        <MyAdListStack.Screen name="my_adlist"
                          component={MyAdListContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    },
                                     animationEnabled: false,
                                     headerTitle : props => 
                                        <HeaderCustom onPress={ () => navigation.goBack() } currentStep={"De quoi s'agit-il ?"} {...props} />, 
                                  }}
        />
    </MyAdListStack.Navigator>
)

export default MyAdListStackNavigator; 