
import React from 'react';
import {View, Button} from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import {RequestCategoryContainer, RequestInfosContainer, 
        RequestConditionContainer, RequestUrgencyContainer, 
        RequestDeliveryContainer, RequestPriceContainer,
        RequestRecapContainer, 
        } from '../containers';
import { HeaderCustom, PreviousArrow, EllipseOptions } from '../components';

const RequestStack = createStackNavigator();

const RequestStackNavigator = ({ navigation }) => (
    <RequestStack.Navigator initialRouteName="request_category">
         <RequestStack.Screen name="request_category"
                          component={RequestCategoryContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    },
                                     animationEnabled: false,
                                     headerTitle : props => 
                                        <HeaderCustom onPress={ () => navigation.goBack() } currentStep={"Que recherchez-vous ?"} {...props} />, 
                                  }}
        />
        <RequestStack.Screen name="request_infos"
                          component={RequestInfosContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    },
                                     animationEnabled: false,
                                     headerTitle : props => 
                                        <HeaderCustom onPress={ () => navigation.goBack() } currentStep={"Que recherchez-vous ?"} {...props} />, 
                                  }}
        />
        <RequestStack.Screen name="request_condition"
                          component={RequestConditionContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Dans quel état ?"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
        <RequestStack.Screen name="request_urgency"
                          component={RequestUrgencyContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Quelle est l'urgence ?"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
        <RequestStack.Screen name="request_delivery"
                          component={RequestDeliveryContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Mode de livraison"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
        <RequestStack.Screen name="request_price"
                          component={RequestPriceContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                       <HeaderCustom style={{ position : 'absolute'}} currentStep={"Prix conseillé"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
        <RequestStack.Screen name="request_recap"
                          component={RequestRecapContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                     elevation: 0,
                                                     shadowOffset: { height: 0 }
                                                    }, 
                                    animationEnabled: false,
                                    headerTitle : props => 
                                        <HeaderCustom style={{ position : 'absolute'}} currentStep={"Récapitulatif"} {...props} />,
                                    
                                    headerLeft: (props) => 
                                       <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                    
                                    headerRight : (props) => 
                                       <EllipseOptions onPress={() => console.log(props)} {...props}/>,
                                 }}
        />
    </RequestStack.Navigator>
)

export default RequestStackNavigator; 