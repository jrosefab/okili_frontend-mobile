import React, { useState, useEffect } from 'react';
import { View, TouchableWithoutFeedback, 
         Text, TouchableOpacity,
         StyleSheet,  Modal } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { RecentAdsContainer, MyAdListContainer, 
         ProposalsContainer, UserContainer, LoginContainer } from '../containers';
import { TabBar, ModalLogin } from '../components';
import { connect } from 'react-redux';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const Tabs = createBottomTabNavigator(); // créer une collection avec bouton de navigation

function BottomStack( props ){
    const { authenticate, navigation } = props
    useEffect(() =>{
      //  props.getUser();
    }, [] );
    
    const [modalVisible, setModalVisible] = useState(false)
    const [isLogged, setIsLogged] = useState(false)

    return (
        <>
            <Tabs.Navigator tabBarOptions = {{
                    showLabel : false,
                    style : { height : 60 }
                }} 
            >
                <Tabs.Screen name="all_ads" 
                             component={RecentAdsContainer}
                             options = {( { route } ) => ({ 
                                tabBarIcon: ({ focused }) => {
                                    let iconColor;
                                    if(route.name === "all_ads"){
                                        iconColor = focused
                                        ? '#7FCBE2'
                                        : '#B2BEC3';
                                    }
                                    return ( 
                                        <TabBar label="Accueil">
                                            <Entypo name="home" size={30} color={iconColor} />
                                        </TabBar>
                                    )
                                }
                            })}
                />
                <Tabs.Screen name="my_adlist" 
                            component={MyAdListContainer} 
                            listeners= {{ tabPress: (e) => {  
                                !authenticate.profileId ?
                                    setIsLogged(true)
                                :   navigation.navigate('my_adlist') 
                            }}}
                            options = {( { route } ) => ({ 
                                tabBarIcon: ({ focused }) => {
                                    let iconColor;
                                    if(route.name === "my_adlist"){
                                        iconColor = focused
                                        ? '#F19CB8'
                                        : '#B2BEC3';
                                    }
                                    return ( 
                                    <TabBar label="Mes annonces">
                                        <Entypo name="megaphone" size={30} color={iconColor} />
                                    </TabBar>
                                    )
                                }
                            })}
                />
                <Tabs.Screen name="create" 
                             component={RecentAdsContainer}
                             listeners= {{ tabPress: (e) => { e.preventDefault(); } }}
                             style={{ position :"relative"}}
                             options = {{ 
                                tabBarIcon : () => (
                                    <TouchableOpacity onPress={() => setModalVisible(true) } >
                                        <TabBar label="Créer">
                                            <Entypo name="circle-with-plus" color="#B2BEC3" size={30} />
                                        </TabBar>
                                    </TouchableOpacity>
                                )
                            }}
                />
                <Tabs.Screen name="proposals" 
                            component={ProposalsContainer} 
                            options = {( { route } ) => ({ 
                                tabBarIcon: ({ focused }) => {
                                    let iconColor;
                                    if(route.name === "proposals"){
                                        iconColor = focused
                                        ? '#E98843'
                                        : '#B2BEC3';
                                    }
                                    return ( 
                                        <TabBar label="Propositions">
                                            <FontAwesome5 name="hands-helping" size={30} color={iconColor} />
                                        </TabBar>
                                    );
                                }
                            })}
                />
                { authenticate.profileId ?
                    <Tabs.Screen name="profile" 
                                component= {UserContainer} 
                                options = {( { route } ) => ({ 
                                    tabBarIcon: ({ focused }) => {
                                        let iconColor;
                                        if(route.name === "profile"){
                                            iconColor = focused
                                            ? '#7FCBE2'
                                            : '#B2BEC3';
                                        }
                                        return ( 
                                            <TabBar label="Profile">
                                                <FontAwesome name="user-circle" size={30} color={iconColor} />
                                            </TabBar>
                                        )
                                    }
                                })}
                    />
                :
                    <Tabs.Screen name="login" 
                                component={LoginContainer}
                                listeners= {{ tabPress: (e) => {  
                                                e.preventDefault(); 
                                                navigation.push('login') } 
                                            }}
                                options = {( { route } ) => ({ 
                                    tabBarIcon: ({ focused }) => {
                                        let iconColor;
                                        if(route.name === "login"){
                                            iconColor = focused
                                            ? '#7FCBE2'
                                            : '#B2BEC3';
                                        }
                                        return ( 
                                            <TabBar label="Connexion">
                                                <FontAwesome name="user-circle" size={30} color={iconColor} />
                                            </TabBar>
                                        )
                                    }
                                })}
                    />
                }
            </Tabs.Navigator>

            <Modal
                animationType="fade"
                transparent={true}
                visible={modalVisible}
                onRequestClose={!modalVisible}
            >
                 <TouchableWithoutFeedback onPress={() => {
                       setModalVisible(!modalVisible);
                  }}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <TouchableOpacity style={styles.categoryContainer} 
                                              onPress={() => { navigation.navigate('sale_category'); 
                                                                setModalVisible(false) }}
                            >
                                <View style={styles.category}>
                                    <FontAwesome5 style={styles.icon} name="money-bill" size={25} />                                    
                                    <Text style={styles.textStyle}>
                                        Je met en vente
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.categoryContainer} 
                                              onPress={() => { navigation.navigate('request_category'); 
                                                                setModalVisible(false) }}
                            >                                
                                <View style={styles.category}>
                                    <FontAwesome5 style={styles.icon} name="binoculars" size={25} />                                    
                                    <Text style={styles.textStyle}>
                                        Je recherche
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
            <ModalLogin 
                    onPress={() => {
                        setIsLogged(!isLogged);
                    }}
                    visible={isLogged}
                    onRequestClose={!isLogged}
                    login={()=>{ 
                        navigation.navigate('login');
                        setIsLogged(false)}
                    }
                    signup={()=>{ 
                        navigation.navigate('login', { screen : 'signup' });
                        setIsLogged(false)
                    }}
            />
        </>
    )
}

const styles = StyleSheet.create({
    centeredView: {
      flex : 1,
      justifyContent : "flex-end",
      marginBottom : 0,
      width : "100%",
      backgroundColor : "rgba(0, 0, 0, 0.72)"
    },
    modalView: {
      padding: 35,
      alignItems: "baseline",
      shadowOffset: {
        width: 0,
        height: 2
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
    },
    categoryContainer : {
        backgroundColor : "#fff",
        width : "100%",
        margin : 5,
        padding : 8,
        borderRadius : 50,
    },
    category : {
        display : "flex",
        flexDirection : "row",
        alignItems : "center"
    },
    icon : {
        margin : 10,
        color: "#B2BEC3",
    },  
    textStyle: {
      color: "#000",
      textAlign : "center",
    },
    modalText: {
      marginBottom: 15,
      textAlign: "center"
    }
  });

  const mapStateToProps = (state) => {
    return {
        user : state.userReducer,
        authenticate : state.authReducer
    }
}

const BottomStackNavigator = connect(mapStateToProps)(BottomStack)

export default BottomStackNavigator;