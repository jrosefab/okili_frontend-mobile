import React from 'react';
import {View, Button} from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import { LoginContainer, SignupContainer } from '../containers';
import { HeaderCustom, PreviousArrow, EllipseOptions } from '../components';

const AuthStack = createStackNavigator();

const config = {
   animation: 'spring',
   config: {
     stiffness: 1000,
     damping: 500,
     mass: 3,
     overshootClamping: true,
     restDisplacementThreshold: 0.01,
     restSpeedThreshold: 0.01,
   },
 };

const AuthStackNavigator = ({ navigation }) => (
    <AuthStack.Navigator initialRouteName="login">
        <AuthStack.Screen name="login"
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                      elevation: 0,
                                                      shadowOffset: { height: 0 }
                                                    },
                                      animationEnabled: false,
                                      
                                      headerTitle : props => 
                                        <HeaderCustom currentStep={"Se connecter"} {...props} />, 
                                  }}
                          component={LoginContainer}
        />
        <AuthStack.Screen name="signup"
                          component={SignupContainer}
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                      elevation: 0,
                                                      shadowOffset: { height: 0 }
                                                    },
                                      animationEnabled: false,
                                      headerLeft: (props) => 
                                        <PreviousArrow onPress={() => navigation.goBack(null)} title="Back" {...props}/>,
                                      
                                      headerTitle : props => 
                                        <HeaderCustom currentStep={"S'inscrire"} {...props} />, 
                                  }}
        />
    </AuthStack.Navigator>
)

export default AuthStackNavigator; 