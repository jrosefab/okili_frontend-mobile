import React from 'react';
import {View, Button} from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import { SingleAdContainer } from '../containers';
import { HeaderCustom, PreviousArrow, EllipseOptions } from '../components';

const SingleAdStack = createStackNavigator();

const SingleAdStackNavigator = ({ navigation }) => (
    <SingleAdStack.Navigator initialRouteName="single_ad">
        <SingleAdStack.Screen name="single_ad"
                          options={{ headerStyle : { backgroundColor: '#fff', 
                                                      elevation: 0,
                                                      shadowOffset: { height: 0 }
                                                    },
                                      animationEnabled: false,
                                      headerShown : false,
                                  }}
                          component={SingleAdContainer}
        />
    </SingleAdStack.Navigator>
)

export default SingleAdStackNavigator; 