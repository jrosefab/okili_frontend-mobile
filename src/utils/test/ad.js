export const ad = {   
    kind: "shopping",
    category: "equipment",
    home_delivery: false, 
    home_pickup: true, 
    home_pickup_details: "Il faut sonner à l'interphone JOSEPH-ROSE", 
    delivery_km_acceptation: 4,
    tags : ["café", "machine  à café", "bio"],
    details_content : "Vélo Décathlon de très bonne qualité",
    quantity_left: null, 
    condition: "bad",
    title: "Capsule de Nesspresso",
    urgency: "slow",
    address: "13 Avenue Robert Schuman, 59370 Mons-en-Barœul, France",
    longitude: 3.1085149999999997,
    latitude: 50.64262166666667,
    price : 0.9,
    picture_publication : {
        uri : "file:///storage/emulated/0/Android/data/com.okili/files/Pictures/image-b218226d-562c-4c6f-82fc-5e3d4d995759.jpg",
        fileName: "fruits.jpg",
        path : "/storage/emulated/0/Android/data/com.okili/files/Pictures/image-b218226d-562c-4c6f-82fc-5e3d4d995759.jpg",
        type: "image/jpeg"
    }
}
