import React, { useEffect } from 'react';
import { Platform } from 'react-native'
import { Provider } from "react-redux";
import SplashScreen from 'react-native-splash-screen'
import MainApp from './src/';
import store from "./src/store";

const App = () => {
  useEffect( ()=>{
    SplashScreen.hide();
  }, []);

  return (
    <Provider store={store}>
      <MainApp/>
    </Provider>
  );
};

export default App;
